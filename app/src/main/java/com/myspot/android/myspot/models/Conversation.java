package com.myspot.android.myspot.models;

import android.os.Parcel;
import android.os.Parcelable;

public class Conversation implements Parcelable {
    private String date;
    private String personA, personB;
    private String messages;
    private String lastMessage;
    private String lastMessageSender;
    private boolean lastMessageRead;

    public Conversation() {
        lastMessageRead = false;
    }

    public Conversation(Conversation copy) {
        this.date = copy.getDate();
        this.personA = copy.getPersonA();
        this.personB = copy.getPersonB();
        this.messages = copy.getMessages();
        this.lastMessageRead = copy.isLastMessageRead();
        this.lastMessage = copy.getLastMessage();
        this.lastMessageSender = copy.getLastMessageSender();
    }

    public String getOtherUser(String uId) {
        return personA.equals(uId)
                ? personB
                : personA;
    }

    public String getLastMessageSender() {
        return lastMessageSender;
    }

    public void setLastMessageSender(String lastMessageSender) {
        this.lastMessageSender = lastMessageSender;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getPersonA() {
        return personA;
    }

    public void setPersonA(String personA) {
        this.personA = personA;
    }

    public String getPersonB() {
        return personB;
    }

    public void setPersonB(String personB) {
        this.personB = personB;
    }

    public String getMessages() {
        return messages;
    }

    public void setMessages(String messages) {
        this.messages = messages;
    }

    public String getLastMessage() {
        return lastMessage;
    }

    public void setLastMessage(String lastMessage) {
        this.lastMessage = lastMessage;
    }

    public boolean isLastMessageRead() {
        return lastMessageRead;
    }

    public void setLastMessageRead(boolean lastMessageRead) {
        this.lastMessageRead = lastMessageRead;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.date);
        dest.writeString(this.personA);
        dest.writeString(this.personB);
        dest.writeString(this.messages);
        dest.writeString(this.lastMessage);
        dest.writeString(this.lastMessageSender);
        dest.writeByte(this.lastMessageRead ? (byte) 1 : (byte) 0);
    }

    protected Conversation(Parcel in) {
        this.date = in.readString();
        this.personA = in.readString();
        this.personB = in.readString();
        this.messages = in.readString();
        this.lastMessage = in.readString();
        this.lastMessageSender = in.readString();
        this.lastMessageRead = in.readByte() != 0;
    }

    public static final Parcelable.Creator<Conversation> CREATOR = new Parcelable.Creator<Conversation>() {
        @Override
        public Conversation createFromParcel(Parcel source) {
            return new Conversation(source);
        }

        @Override
        public Conversation[] newArray(int size) {
            return new Conversation[size];
        }
    };
}
