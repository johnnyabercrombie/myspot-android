package com.myspot.android.myspot.utils;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.support.annotation.ColorRes;
import android.support.annotation.DrawableRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.DisplayMetrics;
import android.util.Log;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.storage.FileDownloadTask;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.myspot.android.myspot.models.Booking;
import com.myspot.android.myspot.models.Listing;
import com.myspot.android.myspot.models.Spot;

import java.io.File;
import java.text.DateFormat;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Locale;
import java.util.Set;

public class Util {
    public static final int REQ_FINE_LOCATION = 99;
    public static final String EMAIL_REGEX = "^[\\w-_\\.+]*[\\w-_\\.]\\@([\\w]+\\.)+[\\w]+[\\w]$";
    private static final String TAG = Util.class.getSimpleName();

    public static final DateFormat rangeDateFormat = new SimpleDateFormat("MM/dd/yyyy", Locale.getDefault());
    public static final DateFormat rangeTimeFormat = new SimpleDateFormat("HH:mm", Locale.getDefault());
    @SuppressLint("SimpleDateFormat")
    public static final DateFormat firebaseDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

    public static final DateFormat defaultDateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss", Locale.getDefault());

    public static Date parseDate(String dateString) {
        //// TODO: 1/22/2017 Create date parsing logic
        return new Date(9053653);
    }

    public static boolean isSignedIn() {
        return FirebaseAuth.getInstance().getCurrentUser() != null
                && !FirebaseAuth.getInstance().getCurrentUser().isAnonymous();
    }

    /**
     * Converts a cost into a US Currency String
     * @param cost represented in normal format (i.e. 200 = $2.00)
     * @return currency string
     */
    public static String parseCost(long cost) {
        NumberFormat format = NumberFormat.getCurrencyInstance(Locale.US);

        return format.format(cost / 100.0);
    }

    public static String parseAddress(Spot.Address address) {
        return address.getStreet() + ",\n" + address.getCity() + " " + address.getPostalCode();
    }

    public static int dpToPx(int dp, Context context) {
        DisplayMetrics displayMetrics = context.getResources().getDisplayMetrics();
        return Math.round(dp * (displayMetrics.xdpi / DisplayMetrics.DENSITY_DEFAULT));
    }

    public static Drawable getDrawableCompat(Context context, @DrawableRes int drawableRes) {
        return Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP
                ? context.getDrawable(drawableRes)
                : context.getResources().getDrawable(drawableRes);
    }

    public static int getColorCompat(Context context, @ColorRes int color) {
        return Build.VERSION.SDK_INT >= Build.VERSION_CODES.M
                ? context.getColor(color)
                : context.getResources().getColor(color);
    }

    public static String[] getNameStrings(String name) {
        if (name != null) {
            String[] values = name.trim().split(" ", 2);
            String firstName = values[0];
            String lastName;
            if (values[values.length-1].equals(values[0])) {
                lastName = "";
            } else {
                lastName = values[1];
            }
            Log.d(TAG, "getNameStrings: firstName = " + firstName + " and lastName = " + lastName);

            return new String[]{firstName, lastName};
        }else {
            return new String[2];
        }
    }

    public static void getProfilePic(String uID, @Nullable OnProfileSuccessListener onSuccessListener, @Nullable OnFailureListener onFailureListener) {
        StorageReference ref = FirebaseStorage.getInstance()
                .getReference("users")
                .child(uID)
                .child("prof_image.jpg");
        try {
            final File localFile = File.createTempFile("prof", "jpg");
            ref.getFile(localFile)
                    .addOnSuccessListener(onSuccessListener != null
                            ? onSuccessListener.with(localFile).with(uID)
                            : new OnSuccessListener<FileDownloadTask.TaskSnapshot>() {
                        @Override
                        public void onSuccess(FileDownloadTask.TaskSnapshot taskSnapshot) {}})
                    .addOnFailureListener(onFailureListener != null ? onFailureListener : new OnFailureListener() {
                        @Override
                        public void onFailure(@NonNull Exception e) {    }
                    });

        } catch (Exception e) {
            Log.e(TAG, "getProfilePic: " + Log.getStackTraceString(e));
        }
    }

    public static String getMessageDateFormatted(String date) {
        // TODO: 5/16/2017 format date properly

        return date;
    }

    public static String defaultUntilDate() {
        Calendar c = Calendar.getInstance();
        c.setTime(new Date());
        c.add(Calendar.DATE, 2);

        return rangeDateFormat.format(c.getTime());
    }

    public static String defaultUntilTime() {
        Calendar c = Calendar.getInstance();
        c.setTime(new Date());
        c.add(Calendar.DATE, 2);

        return rangeTimeFormat.format(c.getTime());
    }

    public static void purchaseListing(Listing newListing, String listingKey) {
        Booking booking = new Booking(newListing, FirebaseAuth.getInstance().getCurrentUser().getUid());
        String bookingUID = FirebaseDatabase.getInstance().getReference("bookings").push().getKey();
        FirebaseDatabase.getInstance().getReference("bookings").child(bookingUID).setValue(booking);
        FirebaseDatabase.getInstance().getReference("users")
                .child(booking.getOwner())
                .child("bookings").push().setValue(bookingUID);
        FirebaseDatabase.getInstance().getReference("users")
                .child(booking.getBuyer())
                .child("purchases").push().setValue(bookingUID);
        FirebaseDatabase.getInstance().getReference("geofire")
                .child(listingKey).removeValue();
    }

    public static abstract class OnProfileSuccessListener implements OnSuccessListener<FileDownloadTask.TaskSnapshot> {
        private File localFile;
        private String uID;

        public File getLocalFile() {
            return localFile;
        }

        public String getuID() {
            return uID;
        }

        public OnProfileSuccessListener with(File file) {
            this.localFile = file;
            return this;
        }

        public OnProfileSuccessListener with(String uID) {
            this.uID = uID;
            return this;
        }

        public void cleanUp() {
            localFile = null;
        }
    }

    public static class BiMap<T1, T2> {
        public static class Entry<T1, T2>
        {
            public final T1 item1;
            public final T2 item2;

            public Entry( T1 item1, T2 item2 )
            {
                this.item1 = item1;
                this.item2 = item2;
            }
        }

        private final HashMap<Object, Entry<T1, T2>> mappings = new HashMap<>();
        private int loopedLinkCount;

        private boolean areEqual(T1 item1, T2 item2) {
            if (item1 == null) {
                return item2 == null;
            } else {
                return item2 != null && item1.equals(item2);
            }
        }

        public void put( T1 item1, T2 item2 ) {
            remove(item1);
            remove(item2);
            Entry<T1, T2> entry = new Entry<T1, T2>( item1, item2 );
            mappings.put(item1, entry );
            mappings.put(item2, entry );
            if(areEqual(item1, item2)){
                loopedLinkCount++;
            }
        }

        /**
         *
         * @param key
         * @return an entry containing the key and it's one to one mapping or null if there is
         * no mapping for the key.
         */
        public Entry<T1, T2> get(Object key)
        {
            return mappings.get(key);
        }

        public Entry<T1, T2> remove( Object key )
        {
            Entry<T1, T2> entry = mappings.remove( key );
            if( entry == null ){
                return null;
            }
            if( areEqual(entry.item1, entry.item2) ){
                loopedLinkCount--;
                return entry;
            }
            return mappings.remove( areEqual(entry.item1, entry.item2)  ?  entry.item2  :  entry.item1 );
        }

        public Set< Entry<T1, T2> > entrySet()
        {
            return new HashSet<>( mappings.values() );
        }

        public int size()
        {
            return ( mappings.size() + loopedLinkCount ) / 2;
        }
    }
}
