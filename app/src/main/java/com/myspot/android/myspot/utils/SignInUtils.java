package com.myspot.android.myspot.utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.support.annotation.NonNull;
import android.util.Log;

import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.EmailAuthProvider;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.myspot.android.myspot.fragments.SignInFragment;
import com.myspot.android.myspot.models.User;

import static com.myspot.android.myspot.utils.Constants.USER_PREFS;
import static com.myspot.android.myspot.utils.Constants.USER_PREFS_EMAIL;
import static com.myspot.android.myspot.utils.Constants.USER_PREFS_MAP_TYPE;
import static com.myspot.android.myspot.utils.Constants.USER_PREFS_NAME;
import static com.myspot.android.myspot.utils.Constants.USER_PREFS_PROFILE_URL;
import static com.myspot.android.myspot.utils.Constants.USER_PREFS_UID;

public class SignInUtils {

    private static final String TAG = SignInUtils.class.getSimpleName();

    public static void updateUserDB(final String uid, final SharedPreferences prefs) {
        final FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();

        prefs.edit().putString(USER_PREFS_UID, uid).apply();
        FirebaseDatabase.getInstance().getReference("users").child(uid).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if (dataSnapshot.getValue() == null) {
                    Log.d(TAG, "onDataChange: value was null! Adding new user");
                    if (user != null) {
                        String[] splitName = Util.getNameStrings(user.getDisplayName());


                        FirebaseDatabase.getInstance()
                                .getReference("users")
                                .child(uid)
                                .setValue(new User()
                                        .setInfo(new User.Info()
                                                .setFirstName(splitName[0])
                                                .setLastName(splitName[1]))
                                        .setSettings(new User.Settings()
                                                .setMapType(prefs.getString(USER_PREFS_MAP_TYPE, "Standard"))))
                                .addOnCompleteListener(new OnCompleteListener<Void>() {
                                    @Override
                                    public void onComplete(@NonNull Task<Void> task) {
                                        if (task.isSuccessful()) {
                                            Log.d(TAG, "onDataChange: task success!");
                                        } else {
                                            Log.d(TAG, "onDataChange: task failed!" + Log.getStackTraceString(task.getException()));
                                        }
                                    }
                                });
                    } else {
                        Log.e(TAG, "onDataChange: user was null!");
                    }
                } else {
                    Log.d(TAG, "onDataChange: value was not null! " + dataSnapshot.getValue());
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                Log.e(TAG, "onCancelled: " + Log.getStackTraceString(databaseError.toException()) );
            }
        });
    }

    public static class OnGoogleSignInConnectFailedListener implements GoogleApiClient.OnConnectionFailedListener {
        private static final String TAG = OnGoogleSignInConnectFailedListener.class.getSimpleName();

        private SignInFragment.OnSignInFragmentInteractionListener listener;

        @Override
        public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
            if (listener != null)
                listener.onSignInCompleted(false);
        }

        public void bind(SignInFragment.OnSignInFragmentInteractionListener listener) {
            this.listener = listener;
        }

        public void unbind() {
            listener = null;
        }
    }

    public static class OnSignUpCompleteListener implements OnCompleteListener<AuthResult> {
        private static final String TAG = OnSignUpCompleteListener.class.getSimpleName();
        private String emailText, passText;
        private FirebaseUser prevUser;
        private Context context;
        private SignInFragment.OnSignInFragmentInteractionListener listener;

        @Override
        public void onComplete(@NonNull Task<AuthResult> task) {
            if (context == null || emailText == null || passText == null) {
                Log.w(TAG, "onComplete: null parameters");
                return;
            }

            if (task.isSuccessful()) {
                AuthCredential credential = EmailAuthProvider.getCredential(emailText, passText);
                if (prevUser != null && prevUser.isAnonymous()) {
                    prevUser.linkWithCredential(credential)
                            .addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                                @Override
                                public void onComplete(@NonNull Task<AuthResult> task) {
                                    if (task.isSuccessful()) {
                                        Log.d(TAG, "onComplete: link good!");
                                        if (context != null) {

                                        }
                                    }
                                    else
                                        Log.d(TAG, "onComplete: link failed! "
                                                + Log.getStackTraceString(task.getException()));
                                }
                            });
                }
            }
            if (listener != null)
                listener.onSignInCompleted(task.isSuccessful());
        }

        public void bind(String emailText, String passText, FirebaseUser prevUser,
                         SignInFragment.OnSignInFragmentInteractionListener listener, Context context) {
            this.emailText = emailText;
            this.passText = passText;
            this.prevUser = prevUser;
            this.listener = listener;
            this.context = context;
        }

        public void unBind() {
            emailText = null;
            passText = null;
            prevUser = null;
            listener = null;
            context = null;
        }
    }

    public static class OnSignInCompleteListener implements OnCompleteListener<AuthResult> {
        private static final String TAG = OnSignInCompleteListener.class.getSimpleName();

        private SignInFragment.OnSignInFragmentInteractionListener listener;

        @Override
        public void onComplete(@NonNull Task<AuthResult> task) {
            if (!task.isSuccessful())
                Log.e(TAG, "onComplete: " + Log.getStackTraceString(task.getException()));

            if (listener != null)
                listener.onSignInCompleted(task.isSuccessful());
        }

        public void bind(SignInFragment.OnSignInFragmentInteractionListener listener) {
            this.listener = listener;
        }

        public void unBind() {
            listener = null;
        }
    }

    public static void saveGoogleProfInfo(@NonNull GoogleSignInResult result, Context context) {
        Log.d(TAG, "onActivityResult: profile url = " + result.getSignInAccount().getPhotoUrl());
        if (result.getSignInAccount().getPhotoUrl() != null) {
            String photoUrl = result.getSignInAccount().getPhotoUrl().toString().replace("/s96-c/", "/s1024-c/");
            SharedPreferences sharedPreferences = context.getSharedPreferences(USER_PREFS, Context.MODE_PRIVATE);
            sharedPreferences.edit()
                    .putString(USER_PREFS_PROFILE_URL, photoUrl)
                    .putString(USER_PREFS_EMAIL, result.getSignInAccount().getEmail())
                    .putString(USER_PREFS_NAME, result.getSignInAccount().getDisplayName())
                    .apply();
        }
    }

    public static void signOut(SharedPreferences sharedPreferences) {
        FirebaseAuth.getInstance().signOut();
        FirebaseAuth.getInstance().signInAnonymously();
        sharedPreferences.edit()
                .remove(USER_PREFS_PROFILE_URL)
                .remove(USER_PREFS_UID)
                .remove(USER_PREFS_NAME)
                .remove(USER_PREFS_EMAIL)
                .apply();
    }
}
