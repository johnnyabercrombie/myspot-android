package com.myspot.android.myspot;

import android.content.DialogInterface;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.design.widget.BottomSheetBehavior;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.Toast;

import com.esafirm.imagepicker.model.Image;
import com.google.firebase.auth.FirebaseAuth;
import com.myspot.android.myspot.databinding.ActivityMainBinding;
import com.myspot.android.myspot.fragments.ListASpotFragment;
import com.myspot.android.myspot.fragments.MapsFragment;
import com.myspot.android.myspot.fragments.MessagesFragment;
import com.myspot.android.myspot.fragments.ProfileFragment;
import com.myspot.android.myspot.fragments.SignInFragment;
import com.myspot.android.myspot.models.Listing;
import com.myspot.android.myspot.utils.BottomNavigationViewHelper;
import com.myspot.android.myspot.utils.SignInUtils;
import com.myspot.android.myspot.utils.Util;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

import static com.myspot.android.myspot.utils.Constants.USER_PREFS;

public class MainActivity extends AppCompatActivity
        implements SignInFragment.OnSignInFragmentInteractionListener,
        MapsFragment.OnMapsFragmentInteractionListener {
    private static final String TAG = MainActivity.class.getSimpleName();


    private ActivityMainBinding mBinding;

    private SavedData mSavedData;

    private Fragment mCurrentFragment;
    private MapsFragment mMapsFragment;
    private SignInFragment mSignInFragment;
    private ProfileFragment mProfileFragment;
    private MessagesFragment mMessageFragment;
    private ListASpotFragment mListASpotFragment;
    private BottomSheetBehavior mBottomSheetBehavior;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_main);
        mBottomSheetBehavior = BottomSheetBehavior.from(mBinding.selectedContent.bottomSheetViewGroup);
        mBottomSheetBehavior.setBottomSheetCallback(new BottomSheetBehavior.BottomSheetCallback() {
            @Override
            public void onStateChanged(@NonNull View bottomSheet, int newState) {
                if (newState == BottomSheetBehavior.STATE_EXPANDED) {
                    mSavedData.isSelectedExpanded = true;
                } else {
                    mSavedData.isSelectedExpanded = false;
                }
            }

            @Override
            public void onSlide(@NonNull View bottomSheet, float slideOffset) {

            }
        });
        mBottomSheetBehavior.setHideable(true);

        mSavedData = (SavedData) getLastCustomNonConfigurationInstance();
        if (mSavedData == null) {
            mSavedData = new SavedData();
            mSavedData.selectedTab = R.id.map;
            mSavedData.dateFrom = new Date();
            Calendar c = Calendar.getInstance();
            c.setTime(mSavedData.dateFrom);
            c.add(Calendar.DATE, 2);
            mSavedData.dateUntil = c.getTime();
            mSavedData.isSelectedExpanded = false;
        }

        if (mSavedData.isSelectedExpanded && mSavedData.selectedListing != null) {
            showListingDetails(mSavedData.selectedListing, mSavedData.selectedListingId);
        } else {
            mBottomSheetBehavior.setState(BottomSheetBehavior.STATE_HIDDEN);
        }

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            getWindow().addFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            getWindow().addFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_NAVIGATION);
        }

        setupBottomNav();
        setupFragment();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    private void setupFragment() {
        switch (mSavedData.selectedTab) {
            case R.id.map : //Map
                if (mMapsFragment == null) {
                    mMapsFragment = MapsFragment.newInstance(mSavedData.dateFrom, mSavedData.dateUntil);
                }
                mCurrentFragment = mMapsFragment;
                getSupportFragmentManager().beginTransaction()
                        .replace(R.id.frag_container, mCurrentFragment, "MAP_FRAG")
                        .commit();
                break;
            case R.id.messages:
                //mBottomSheetBehavior.setState(BottomSheetBehavior.STATE_HIDDEN);
                if (Util.isSignedIn()) {
                    if (mMessageFragment == null) {
                        mMessageFragment = MessagesFragment.newInstance();
                    }
                    mCurrentFragment = mMessageFragment;
                    getSupportFragmentManager().beginTransaction()
                            .replace(R.id.frag_container, mCurrentFragment, "MESSAGE_FRAG")
                            .commit();
                } else {showSignInFragment();}
                break;
            case R.id.list_a_spot : //List
                //mBottomSheetBehavior.setState(BottomSheetBehavior.STATE_HIDDEN);
                if (Util.isSignedIn()) {
                    if (mListASpotFragment == null) {
                        mListASpotFragment = ListASpotFragment.newInstance();
                    }
                    if (mSavedData.listingDateFrom != null)
                        mListASpotFragment.setdateFrom(mSavedData.listingDateFrom);
                    if (mSavedData.listingDateUntil != null)
                        mListASpotFragment.setDateUntil(mSavedData.listingDateUntil);
                    mCurrentFragment = mListASpotFragment;
                    getSupportFragmentManager().beginTransaction()
                            .replace(R.id.frag_container, mCurrentFragment, "LIST_SPOT_FRAG")
                            .commit();
                } else {showSignInFragment();}
                break;
            case R.id.profile : //Profile
                //mBottomSheetBehavior.setState(BottomSheetBehavior.STATE_HIDDEN);
                if (Util.isSignedIn()) {
                    if (mProfileFragment == null) {
                        mProfileFragment = ProfileFragment.newInstance();
                    }
                    mCurrentFragment = mProfileFragment;
                    getSupportFragmentManager().beginTransaction()
                            .replace(R.id.frag_container, mCurrentFragment, "PROFILE_FRAG")
                            .commit();
                } else {showSignInFragment();}
                break;
        }
    }

    private void showSignInFragment() {
        if (mSignInFragment == null) {
            mSignInFragment = SignInFragment.newInstance();
        }
        mCurrentFragment = mSignInFragment;
        getSupportFragmentManager().beginTransaction()
                .replace(R.id.frag_container, mCurrentFragment, "CURRENT_FRAG")
                .commit();
    }

    // TODO: 2/23/2017 change this to fragment version so hacky workaround is not needed
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (mCurrentFragment != null && mCurrentFragment.getClass() == MapsFragment.class) {
            mCurrentFragment.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }

    @Override
    public Object onRetainCustomNonConfigurationInstance() {
        if (mCurrentFragment instanceof ListASpotFragment) {
            mSavedData.listingDateFrom = mListASpotFragment.getListingDateFrom();
            mSavedData.listingDateUntil = mListASpotFragment.getListingDateUntil();
            mSavedData.listingSpotAddressOne = mListASpotFragment.getListingSpotAddressOne();
            mSavedData.listingPrice = mListASpotFragment.getListingPrice();
            mSavedData.listingRate = mListASpotFragment.getListingRate();
            // TODO: 5/23/2017 save actual location
        } else if (mCurrentFragment instanceof MapsFragment) {
            mSavedData.selectedListing = mMapsFragment.getSelectedListing();
            mSavedData.selectedListingId = mMapsFragment.getSelectedListingId();
        }
        return mSavedData;
    }

    private void setupBottomNav() {
//        AHBottomNavigationItem mapItem = new AHBottomNavigationItem(R.string.map, R.drawable.ic_map_white_24dp, R.color.color_map);
//        AHBottomNavigationItem historyItem = new AHBottomNavigationItem(R.string.purchase_history, R.drawable.ic_history_white_24dp, R.color.color_history);
//        AHBottomNavigationItem listItem = new AHBottomNavigationItem(R.string.list_a_spot, R.drawable.ic_add_location_white_24dp, R.color.color_list);
//        AHBottomNavigationItem profile = new AHBottomNavigationItem(R.string.my_profile, R.drawable.ic_person_outline_white_24dp, R.color.color_profile);

        mBinding.bottomNavigation.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                if (item.getItemId() != mSavedData.selectedTab) {
                    mSavedData.selectedTab = item.getItemId();
                    setupFragment();
                }
                return true;
            }
        });
        mBinding.bottomNavigation.setSelectedItemId(mSavedData.selectedTab);
        BottomNavigationViewHelper.disableShiftMode(mBinding.bottomNavigation);
//        // Add items
//        mBinding.bottomNavigation.addItem(mapItem);
//        mBinding.bottomNavigation.addItem(historyItem);
//        mBinding.bottomNavigation.addItem(listItem);
//        mBinding.bottomNavigation.addItem(profile);
//
//        mBinding.bottomNavigation.setAccentColor(R.color.colorAccent);
//        mBinding.bottomNavigation.setInactiveColor(Color.parseColor("#747474"));
//
//        mBinding.bottomNavigation.setForceTint(true);
//        mBinding.bottomNavigation.setTranslucentNavigationEnabled(true);
//        mBinding.bottomNavigation.setTitleState(AHBottomNavigation.TitleState.SHOW_WHEN_ACTIVE);
//
//        // Use colored navigation with circle reveal effect
//        mBinding.bottomNavigation.setColored(true);
//
//        // Set current item programmatically
//        mBinding.bottomNavigation.setCurrentItem(mSavedData.selectedTab);
//
//        // Customize notification (title, background, typeface)
//        mBinding.bottomNavigation.setNotificationBackgroundColor(Color.parseColor("#F63D2B"));
//
//        // Add or remove notification for each item
////        mBinding.bottomNavigation.setNotification("1", 3);
//        // OR
////        AHNotification notification = new AHNotification.Builder()
////                .setText("1")
////                .setBackgroundColor(ContextCompat.getColor(this, R.color.color_text_primary))
////                .setTextColor(ContextCompat.getColor(this, R.color.color_text_secondary))
////                .build();
////        mBinding.bottomNavigation.setNotification(notification, 1);
//
//        // Set listeners
//        mBinding.bottomNavigation.setOnTabSelectedListener(new AHBottomNavigation.OnTabSelectedListener() {
//            @Override
//            public boolean onTabSelected(int position, boolean wasSelected) {
//                if (position != mSavedData.selectedTab) {
//                    mSavedData.selectedTab = position;
//                    setupFragment();
//                    setupSearchBar();
//                }
//                return true;
//            }
//        });
//        mBinding.bottomNavigation.setOnNavigationPositionListener(new AHBottomNavigation.OnNavigationPositionListener() {
//            @Override public void onPositionChange(int y) {
//                // Manage the new y position
//            }
//        });
    }

    @Override
    public void onSignInStarted() {
        mBinding.contentLoading.loadingView.setVisibility(View.VISIBLE);
    }

    @Override
    public void onSignInCompleted(boolean success) {
        mBinding.contentLoading.loadingView.setVisibility(View.GONE);
        if (success) {
            Snackbar.make(mBinding.bottomNavigation, R.string.succ_sign_in, Snackbar.LENGTH_SHORT)
                    .show();
            String uid = FirebaseAuth.getInstance().getCurrentUser().getUid();
            SignInUtils.updateUserDB(uid, getSharedPreferences(USER_PREFS, MODE_PRIVATE));
            setupFragment();
        } else {
            new AlertDialog.Builder(this).setTitle(R.string.err).setMessage(R.string.err_sign_in)
                    .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    })
                    .setCancelable(false)
                    .show();
        }
    }

    @Override
    public void onMapReady() {

    }

    @Override
    public void onBackPressed() {
        if (mCurrentFragment instanceof MapsFragment && ((MapsFragment) mCurrentFragment).onBackPressed()) {
            Log.d(TAG, "onBackPressed: Consumed back press");
        } else {
            super.onBackPressed();
        }
    }

    public void setSelectedNavItem(int id) {
        mSavedData.selectedTab = id;
        mBinding.bottomNavigation.setSelectedItemId(mSavedData.selectedTab);
        setupFragment();
    }

    public SavedData getSavedData() {
        return mSavedData;
    }

    public void showListingDetails(final Listing newListing, final String listingKey) {
        Log.d(TAG, "showListingDetails: ");
        if (mBinding != null) {
            mBottomSheetBehavior.setState(BottomSheetBehavior.STATE_EXPANDED);
            mBinding.selectedContent.address.setText(newListing.getLocation().getAddress());
            mBinding.selectedContent.purchase.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Util.purchaseListing(newListing, listingKey);
                    mBottomSheetBehavior.setState(BottomSheetBehavior.STATE_HIDDEN);
                    Toast.makeText(MainActivity.this, R.string.debug_purchase, Toast.LENGTH_SHORT).show();}});
            mBinding.selectedContent.pricing.setText(newListing.getPriceText(this));
            mBinding.selectedContent.selectedAvailFrom.setText(newListing.getAvailableFromText());
            mBinding.selectedContent.selectedAvailUntil.setText(newListing.getAvailableUntilText());
            mBinding.selectedContent.viewImages.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {viewSelectedImages(newListing);}});
            mBinding.selectedContent.selectedDescription.setText(newListing.getDescriptionText(this));
            mBinding.selectedContent.share.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    shareListing(newListing);
                }
            });
            mBinding.selectedContent.contact.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    contactLister(newListing);
                }
            });
            Log.d(TAG, "showListingDetails: ran");
        }
    }

    private void contactLister(Listing newListing) {
        Toast.makeText(this, R.string.todo_no_yet_implemented, Toast.LENGTH_SHORT).show();
    }

    private void shareListing(Listing newListing) {
        // TODO: 5/30/2017
        Toast.makeText(this, R.string.todo_no_yet_implemented, Toast.LENGTH_SHORT).show();
    }

    private void viewSelectedImages(Listing newListing) {
        // TODO: 5/30/2017
        Toast.makeText(this, R.string.todo_no_yet_implemented, Toast.LENGTH_SHORT).show();
    }

    public static class SavedData {
        private int selectedTab;
        private Date dateFrom, dateUntil;
        private Date listingDateFrom, listingDateUntil;
        private String listingSpotAddressOne, listingRate;
        private int listingPrice;
        private List<Image> listingImages;
        private Listing selectedListing;
        private boolean isSelectedExpanded;
        public String selectedListingId;



        public Listing getSelectedListing() {
            return selectedListing;
        }

        public void setSelectedListing(Listing selectedListing) {
            this.selectedListing = selectedListing;
        }

        public boolean isSelectedExpanded() {
            return isSelectedExpanded;
        }

        public void setSelectedExpanded(boolean selectedExpanded) {
            isSelectedExpanded = selectedExpanded;
        }

        public Date getDateUntil() {
            return dateUntil;
        }

        public Date getDateFrom() {
            return dateFrom;
        }

        public int getSelectedTab() {
            return selectedTab;
        }

        public void setSelectedTab(int selectedTab) {
            this.selectedTab = selectedTab;
        }

        public void setDateFrom(Date dateFrom) {
            this.dateFrom = dateFrom;
        }

        public void setDateUntil(Date dateUntil) {
            this.dateUntil = dateUntil;
        }

        public Date getListingDateFrom() {
            return listingDateFrom;
        }

        public void setListingDateFrom(Date listingDateFrom) {
            this.listingDateFrom = listingDateFrom;
        }

        public Date getListingDateUntil() {
            return listingDateUntil;
        }

        public void setListingDateUntil(Date listingDateUntil) {
            this.listingDateUntil = listingDateUntil;
        }

        public String getListingSpotAddressOne() {
            return listingSpotAddressOne;
        }

        public void setListingSpotAddressOne(String listingSpotAddressOne) {
            this.listingSpotAddressOne = listingSpotAddressOne;
        }

        public String getListingRate() {
            return listingRate;
        }

        public void setListingRate(String listingRate) {
            this.listingRate = listingRate;
        }

        public int getListingPrice() {
            return listingPrice;
        }

        public void setListingPrice(int listingPrice) {
            this.listingPrice = listingPrice;
        }

        public List<Image> getListingImages() {
            return listingImages;
        }

        public void setListingImages(List<Image> listingImages) {
            this.listingImages = listingImages;
        }
    }
}
