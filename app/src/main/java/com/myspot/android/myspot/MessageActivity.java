package com.myspot.android.myspot;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.text.TextUtils;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewAnimationUtils;
import android.view.ViewTreeObserver;
import android.view.animation.AccelerateInterpolator;

import com.firebase.ui.database.ChangeEventListener;
import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FileDownloadTask;
import com.myspot.android.myspot.databases.ProfileImageDatabase;
import com.myspot.android.myspot.databinding.ActivityMessageBinding;
import com.myspot.android.myspot.models.Conversation;
import com.myspot.android.myspot.models.Message;
import com.myspot.android.myspot.models.User;
import com.myspot.android.myspot.utils.Constants;
import com.myspot.android.myspot.utils.MessageUtil;
import com.myspot.android.myspot.utils.Util;
import com.myspot.android.myspot.view_holders.MessageHolder;

import java.io.File;

public class MessageActivity extends AppCompatActivity {
    private static final String TAG = MessageActivity.class.getSimpleName();

    public static final String EXTRA_CONVERSATION = "EXTRA_CONVERSATION";
    public static final String EXTRA_CONV_ID = "EXTRA_CONV_ID";
    public static final String EXTRA_OTHER_NAME = "EXTRA_OTHER_NAME";
    public static final String EXTRA_CIRCULAR_REVEAL_X = "CIRC_REVEAL_X";
    public static final String EXTRA_CIRCULAR_REVEAL_Y = "CIRC_REVEAL_Y";

    private ValueEventListener mNameListener = new ValueEventListener() {
        @Override
        public void onDataChange(DataSnapshot dataSnapshot) {
            User.Info info = dataSnapshot.getValue(User.Info.class);
            userName = info.getFirstName() + " " + info.getLastName();
            if (mBinding != null && getSupportActionBar() != null) {
                getSupportActionBar().setTitle(userName);
            }
        }

        @Override
        public void onCancelled(DatabaseError databaseError) {

        }
    };

    private ActivityMessageBinding mBinding;
    private String userName;
    private String conversationID;
    private String uID;
    private Conversation mConversation;
    private Util.OnProfileSuccessListener mNonUserProfileListener = new Util.OnProfileSuccessListener() {
        @Override
        public void onSuccess(FileDownloadTask.TaskSnapshot taskSnapshot) {mSavedData.nonUserImage = getLocalFile();}};
    private SavedData mSavedData;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_message);
        final Intent intent = getIntent();
        if (savedInstanceState == null && Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP &&
                intent.hasExtra(EXTRA_CIRCULAR_REVEAL_X) &&
                intent.hasExtra(EXTRA_CIRCULAR_REVEAL_Y)) {
            ViewTreeObserver vto = mBinding.getRoot().getViewTreeObserver();
            if (vto.isAlive()) {
                vto.addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
                    @Override
                    public void onGlobalLayout() {
                        showReveal(intent.getIntExtra(EXTRA_CIRCULAR_REVEAL_X, 0), intent.getIntExtra(EXTRA_CIRCULAR_REVEAL_Y, 0));
                        mBinding.getRoot().getViewTreeObserver().removeOnGlobalLayoutListener(this);
                    }
                });
            }
        } else {
            mBinding.getRoot().setVisibility(View.VISIBLE);
        }



        userName = getSharedPreferences(Constants.USER_PREFS, MODE_PRIVATE).getString(Constants.USER_PREFS_NAME, null);
        mConversation = getIntent().getParcelableExtra(EXTRA_CONVERSATION);
        conversationID = getIntent().getStringExtra(EXTRA_CONV_ID);
        uID = FirebaseAuth.getInstance().getCurrentUser().getUid();
        String otherUserName = getIntent().getStringExtra(EXTRA_OTHER_NAME);
        setSupportActionBar(mBinding.toolbar);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        mSavedData = (SavedData) getLastCustomNonConfigurationInstance();
        if (mSavedData == null) {
            mSavedData = new SavedData();
            setupAdapter();
            File nonUserFile = ProfileImageDatabase.getProfileImage(mConversation.getOtherUser(uID));
            if (nonUserFile != null) {
                mSavedData.nonUserImage = nonUserFile;
            } else {
                Util.getProfilePic(mConversation.getOtherUser(uID), mNonUserProfileListener, null);
            }
        }

        if (otherUserName == null) {
            getOtherUserName();
        } else {
            setTitle(otherUserName);
        }

        mBinding.messages.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));
        mBinding.messages.setAdapter(mSavedData.adapter);
        mBinding.sendButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onSendMessage();
            }
        });

    }

    private void showReveal(int revealX, int revealY) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            float finalRadius = (float) (Math.max(mBinding.getRoot().getWidth(), mBinding.getRoot().getHeight()) * 1.1);

            // create the animator for this view (the start radius is zero)
            Animator circularReveal = ViewAnimationUtils.createCircularReveal(mBinding.getRoot(), revealX, revealY, 0, finalRadius);
            circularReveal.setDuration(400);
            circularReveal.setInterpolator(new AccelerateInterpolator());

            // make the view visible and start the animation
            mBinding.getRoot().setVisibility(View.VISIBLE);
            circularReveal.start();
        } else {
            finish();
        }
    }

    private void showUnReveal(int revealX, int revealY) {
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP) {
            finish();
        } else {
            float finalRadius = (float) (Math.max(mBinding.getRoot().getWidth(), mBinding.getRoot().getHeight()) * 1.1);
            Animator circularReveal = ViewAnimationUtils.createCircularReveal(
                    mBinding.getRoot(), revealX, revealY, finalRadius, 0);

            circularReveal.setDuration(400);
            circularReveal.addListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    mBinding.getRoot().setVisibility(View.INVISIBLE);
                    finish();
                }
            });

            circularReveal.start();
        }
    }


    private void getOtherUserName() {
        FirebaseDatabase.getInstance().getReference("users")
                .child(mConversation.getOtherUser(uID))
                .child("info").addValueEventListener(mNameListener);
    }

    private void onSendMessage() {
        Log.d(TAG, "onSendMessage: registered!");
        String message = mBinding.messageEditText.getText().toString();
        if (!TextUtils.isEmpty(message)) {
            mBinding.sendButton.setEnabled(false);
            MessageUtil.sendMessage(
                    message,
                    userName,
                    mConversation,
                    conversationID,
                    new OnSuccessListener<Void>() {
                        @Override
                        public void onSuccess(Void aVoid) {
                            onMessageSuccess();
                        }
                    },
                    new OnFailureListener() {
                        @Override
                        public void onFailure(@NonNull Exception e) {
                            showFailedMessageSent();
                        }
                    });
        }
    }

    private void onMessageSuccess() {
        mBinding.sendButton.setEnabled(true);
        mBinding.messageEditText.setText("");
        mBinding.messages.smoothScrollToPosition(mSavedData.adapter.getItemCount()-1);
    }

    private void showFailedMessageSent() {
        mBinding.sendButton.setEnabled(true);
        Snackbar.make(mBinding.mainView, R.string.err_message_failed, Snackbar.LENGTH_INDEFINITE)
                .setAction(android.R.string.ok, new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {}})
                .show();
    }

    private void setupAdapter() {
        DatabaseReference ref = FirebaseDatabase.getInstance().getReference("messages").child(mConversation.getMessages());
        mSavedData.adapter = new FirebaseRecyclerAdapter<Message, MessageHolder>(Message.class, R.layout.content_message, MessageHolder.class, ref) {
            @Override
            protected void populateViewHolder(MessageHolder viewHolder, Message model, int position) {
                viewHolder.bind(model, uID,
                        model.getSenderId().equals(uID) ? null : mSavedData.nonUserImage,
                        MessageActivity.this);
            }

            @Override
            public void onViewRecycled(MessageHolder holder) {
                holder.cleanUp();
                super.onViewRecycled(holder);
            }

            @Override
            protected void onDataChanged() {
                if (mBinding != null) {mBinding.messages.smoothScrollToPosition(getItemCount()-1);}
                super.onDataChanged();
            }
        };
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    protected void onDestroy() {
        mSavedData.adapter.cleanup();
        FirebaseDatabase.getInstance()
                .getReference("users")
                .child(mConversation.getOtherUser(uID))
                .child("info")
                .removeEventListener(mNameListener);
        super.onDestroy();
    }

    @Override
    public Object onRetainCustomNonConfigurationInstance() {
        return mSavedData;
    }

    private static class SavedData {
        private FirebaseRecyclerAdapter<Message, MessageHolder> adapter;
        private File nonUserImage;
    }
}
