package com.myspot.android.myspot.fragments;

import android.animation.Animator;
import android.content.Context;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.DecelerateInterpolator;

import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.GoogleAuthProvider;
import com.myspot.android.myspot.R;
import com.myspot.android.myspot.databinding.FragmentSignInBinding;
import com.myspot.android.myspot.utils.SignInUtils;
import com.myspot.android.myspot.utils.Util;

import static com.myspot.android.myspot.utils.Constants.REQUEST_FACEBOOK_SIGN_IN;
import static com.myspot.android.myspot.utils.Constants.REQUEST_GOOGLE_SIGN_IN;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link OnSignInFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link SignInFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class SignInFragment extends Fragment implements View.OnKeyListener {
    private static final String TAG = SignInFragment.class.getSimpleName();
    public static final int PASS_MIN_LENGTH = 6;

    private FragmentSignInBinding mBinding;

    /* Animation Tracking */
    private boolean cardExpanded;
    private boolean cardShowing;
    private boolean transitionedToSignUp;
    private final AlphaAnimation mTitleChangeAlpha = new AlphaAnimation(1f, 0f);
    private final Animation.AnimationListener mTitleChangeAlphaListener = new Animation.AnimationListener() {
        @Override
        public void onAnimationStart(Animation animation) {

        }

        @Override
        public void onAnimationEnd(Animation animation) {
            mBinding.cardContent.title.setText(cardExpanded ? R.string.sign_up : R.string.sign_in);
            AlphaAnimation anim = new AlphaAnimation(0f,1f);
            anim.setDuration(350);
            anim.setInterpolator(new AccelerateInterpolator());
            mBinding.cardContent.title.startAnimation(anim);
        }

        @Override
        public void onAnimationRepeat(Animation animation) {

        }
    };

    private FirebaseAuth mAuth;

    /* Firebase Listeners */
    private SignInUtils.OnSignUpCompleteListener mOnSignUpComplete;
    private SignInUtils.OnSignInCompleteListener mOnSignInComplete;

    private OnSignInFragmentInteractionListener mListener;

    /* Google Sign In */
    private GoogleApiClient mGoogleApiClient;
    private SignInUtils.OnGoogleSignInConnectFailedListener mGoogleFailedListener;

    public SignInFragment() {}

    public static SignInFragment newInstance() {
        SignInFragment fragment = new SignInFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_sign_in, container, false);
        mBinding.cardContent.cardLayout.setVisibility(View.GONE);
        mBinding.noAccountDesc.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {transitionToSignUpView(); showCard();
            }
        });
        mBinding.emailSignIn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {transitionToSignInView(); showCard();
            }
        });
        mBinding.cardContent.signInWithGoogle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {hideCard();
            }
        });
        mBinding.cardContent.noAccount.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {toggleViewLayout();
            }
        });
        mBinding.cardContent.manualAction.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {checkFields();
            }
        });
        mBinding.googleSignIn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                tryGoogleSignIn();
            }
        });

        setupAnimations();

        mBinding.signInLayout.setFocusableInTouchMode(true);
        mBinding.signInLayout.requestFocus();
        mBinding.signInLayout.setOnKeyListener(this);

        mGoogleApiClient = new GoogleApiClient.Builder(getContext())
                .enableAutoManage(getActivity(), mGoogleFailedListener)
                .addApi(Auth.GOOGLE_SIGN_IN_API, new GoogleSignInOptions
                        .Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                        .requestIdToken(getString(R.string.default_web_client_id))
                        .requestEmail()
                        .build())
                .build();

        mOnSignInComplete.bind(mListener);
        return mBinding.signInLayout;
    }

    private void setupAnimations() {
        mTitleChangeAlpha.setDuration(350);
        mTitleChangeAlpha.setInterpolator(new DecelerateInterpolator());
        mTitleChangeAlpha.setAnimationListener(mTitleChangeAlphaListener);
    }

    private void checkFields() {
        boolean success = true;
        final String emailText = mBinding.cardContent.emailEt.getText().toString();
        final String passText = mBinding.cardContent.passwordEt.getText().toString();
        String confirmPassText = mBinding.cardContent.passwordConfirmEt.getText().toString();

        mBinding.cardContent.emailTextLayout.setError(null);
        mBinding.cardContent.passwordConfirmTextLayout.setError(null);
        mBinding.cardContent.passwordTextLayout.setError(null);

        //Check Email
        if (!emailText.matches(Util.EMAIL_REGEX)) {
            mBinding.cardContent.emailTextLayout.setError(getString(R.string.err_invalidEmail));
            success = false;
        }
        if (TextUtils.isEmpty(emailText)) {
            mBinding.cardContent.emailTextLayout.setError(getString(R.string.err_emptyField));
            success = false;
        }
        //Check Password
        if (TextUtils.isEmpty(passText) || passText.length() < PASS_MIN_LENGTH) {
            mBinding.cardContent.passwordTextLayout.setError(getString(R.string.err_invalidPass));
            success = false;
        }
        //Check Confirm Password is signing up
        if (cardExpanded && !passText.equals(confirmPassText)) {
            Log.d(TAG, "checkFields: non matching passwords! (" + passText + ", " + confirmPassText + ")");
            mBinding.cardContent.passwordTextLayout.setError(getString(R.string.err_matchingPass));
            mBinding.cardContent.passwordConfirmTextLayout.setError(getString(R.string.err_matchingPass));
            success = false;
        }

        mListener.onSignInStarted();
        if (success) {
            if (cardExpanded) {
                trySignUp(emailText, passText);
            } else {
                trySignIn(emailText, passText);
            }
        }
    }

    private void trySignUp(final String emailText, final String passText) {
        final FirebaseUser prevUser = mAuth.getCurrentUser();
        mOnSignUpComplete.bind(emailText, passText, prevUser, mListener, getContext());
        mAuth.createUserWithEmailAndPassword(emailText, passText)
                .addOnCompleteListener(mOnSignUpComplete);
    }

    private void trySignIn(String emailText, String passText) {
        mAuth.signInWithEmailAndPassword(emailText, passText)
                .addOnCompleteListener(mOnSignInComplete);
    }

    private void tryGoogleSignIn() {
        mGoogleFailedListener.bind(mListener);
        Auth.GoogleSignInApi.signOut(mGoogleApiClient).setResultCallback(new ResultCallback<Status>() {
            @Override
            public void onResult(@NonNull Status status) {
                Intent signInIntent = Auth.GoogleSignInApi.getSignInIntent(mGoogleApiClient);
                startActivityForResult(signInIntent, REQUEST_GOOGLE_SIGN_IN);
            }
        });
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        Log.d(TAG, "onActivityResult: requestCode = " + requestCode);
        if (requestCode == REQUEST_GOOGLE_SIGN_IN) {
            GoogleSignInResult result = Auth.GoogleSignInApi.getSignInResultFromIntent(data);
            if (result.isSuccess() && result.getSignInAccount() != null && result.getSignInAccount().getIdToken() != null) {
                mAuth.signInWithCredential(GoogleAuthProvider.getCredential(result.getSignInAccount().getIdToken(), null))
                        .addOnCompleteListener(mOnSignInComplete);
                SignInUtils.saveGoogleProfInfo(result, getContext());
            } else {
                mListener.onSignInCompleted(false);
            }
        } else if (requestCode == REQUEST_FACEBOOK_SIGN_IN) {
            // TODO: 5/3/2017 get proper facebook photo url
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mAuth = FirebaseAuth.getInstance();
        mOnSignInComplete = new SignInUtils.OnSignInCompleteListener();
        mOnSignUpComplete = new SignInUtils.OnSignUpCompleteListener();
        mGoogleFailedListener = new SignInUtils.OnGoogleSignInConnectFailedListener();

        if (context instanceof OnSignInFragmentInteractionListener) {
            mListener = (OnSignInFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnSignInFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
        mOnSignUpComplete.unBind();
        mOnSignUpComplete = null;
        mOnSignInComplete.unBind();
        mOnSignInComplete = null;
        mGoogleFailedListener.unbind();
        mGoogleFailedListener = null;
        mGoogleApiClient.disconnect();
        mGoogleApiClient.stopAutoManage(getActivity());
        mGoogleApiClient.disconnect();
    }

    public boolean onBackPressed() {
        if (cardShowing && transitionedToSignUp && cardExpanded) {
            transitionedToSignUp = false;
            transitionToSignInView();
        } else if (cardShowing) {
            hideCard();
        } else {
            return false;
        }

        return true;
    }

    @Override
    public boolean onKey(View v, int keyCode, KeyEvent event) {
        return keyCode == KeyEvent.KEYCODE_BACK && onBackPressed();
    }

    /*
 * ------------------------------
 * ------TRANSITION METHODS------
 * ------------------------------
 */
    private void toggleViewLayout() {
        cardExpanded = !cardExpanded;
        Log.d(TAG, "toggleViewLayout: cardExpanded = " + cardExpanded);
        if (cardExpanded) {
            transitionedToSignUp = true;
            transitionToSignUpView();
        } else {
            transitionedToSignUp = false;
            transitionToSignInView();
        }
    }

    private void hideCard() {
        cardShowing = false;
        transitionedToSignUp = false;
        showMain();
        mBinding.noAccountDesc.setText(R.string.no_account);
        Animation outroAnim = AnimationUtils.loadAnimation(getContext(), R.anim.exit_to_bottom);
        outroAnim.setDuration(350);
        outroAnim.setInterpolator(new AccelerateInterpolator());
        outroAnim.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                if (!cardShowing)
                    mBinding.cardContent.cardLayout.setVisibility(View.GONE);
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });

        mBinding.noAccountDesc.setVisibility(View.VISIBLE);
        mBinding.noAccountDesc.setAlpha(0f);
        mBinding.noAccountDesc.animate().alpha(1f).setDuration(700);
        mBinding.cardContent.cardLayout.startAnimation(outroAnim);
    }

    private void showCard() {
        cardShowing = true;
        hideMain();
        mBinding.noAccountDesc.setAlpha(1f);
        mBinding.noAccountDesc.animate().alpha(0f).setDuration(350).setListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animation) {

            }

            @Override
            public void onAnimationEnd(Animator animation) {
                if (cardShowing)
                    mBinding.noAccountDesc.setVisibility(View.GONE);
            }

            @Override
            public void onAnimationCancel(Animator animation) {

            }

            @Override
            public void onAnimationRepeat(Animator animation) {

            }
        });
        Animation introAnim = AnimationUtils.loadAnimation(getContext(), R.anim.enter_from_bottom);
        introAnim.setDuration(350);
        introAnim.setInterpolator(new DecelerateInterpolator());
        mBinding.cardContent.cardLayout.setVisibility(View.VISIBLE);
        mBinding.cardContent.cardLayout.startAnimation(introAnim);
    }

    private void transitionToSignUpView() {
        cardExpanded = true;
        Log.d(TAG, "transitionToSignUpView: ");
        mBinding.cardContent.title.startAnimation(mTitleChangeAlpha);

        mBinding.cardContent.noAccountDesc.setText(R.string.have_account);
        mBinding.cardContent.noAccountAction.setText(R.string.sign_in);
        mBinding.cardContent.manualAction.setText(R.string.sign_up);

        mBinding.cardContent.forgotPass.setAlpha(1f);
        mBinding.cardContent.forgotPass.animate().alpha(0f).setDuration(350).setListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animation) {

            }

            @Override
            public void onAnimationEnd(Animator animation) {
                if (cardExpanded)
                    mBinding.cardContent.forgotPass.setVisibility(View.GONE);
            }

            @Override
            public void onAnimationCancel(Animator animation) {

            }

            @Override
            public void onAnimationRepeat(Animator animation) {

            }
        });

        mBinding.cardContent.passwordConfirmTextLayout.setVisibility(View.VISIBLE);
        mBinding.cardContent.passwordConfirmTextLayout.setScaleY(0f);
        mBinding.cardContent.passwordConfirmTextLayout.setAlpha(0f);
        mBinding.cardContent.passwordConfirmTextLayout.animate().scaleY(1f).setDuration(350);
        mBinding.cardContent.passwordConfirmTextLayout.animate().alpha(1f).setDuration(350).setListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animation) {

            }

            @Override
            public void onAnimationEnd(Animator animation) {
                if (cardExpanded) {
                    mBinding.cardContent.passwordConfirmTextLayout.setScaleY(1f);
                    mBinding.cardContent.passwordConfirmTextLayout.setAlpha(1f);
                }
            }

            @Override
            public void onAnimationCancel(Animator animation) {

            }

            @Override
            public void onAnimationRepeat(Animator animation) {

            }
        });
    }

    private void transitionToSignInView() {
        Log.d(TAG, "transitionToSignInView: ");
        hideMain();
        cardExpanded = false;
        mBinding.cardContent.title.startAnimation(mTitleChangeAlpha);

        mBinding.cardContent.noAccountDesc.setText(R.string.no_account_no_sign_up);
        mBinding.cardContent.noAccountAction.setText(R.string.sign_up);
        mBinding.cardContent.manualAction.setText(R.string.sign_in);

        mBinding.cardContent.forgotPass.setVisibility(View.VISIBLE);
        mBinding.cardContent.forgotPass.setAlpha(0f);
        mBinding.cardContent.forgotPass.animate().alpha(1f).setDuration(350);

        mBinding.cardContent.passwordConfirmTextLayout.setScaleY(1f);
        mBinding.cardContent.passwordConfirmTextLayout.setAlpha(1f);
        mBinding.cardContent.passwordConfirmTextLayout.animate().scaleY(0f).setDuration(350);
        mBinding.cardContent.passwordConfirmTextLayout.animate().alpha(0f).setDuration(350)
                .setListener(new Animator.AnimatorListener() {
                    @Override
                    public void onAnimationStart(Animator animation) {

                    }

                    @Override
                    public void onAnimationEnd(Animator animation) {
                        if (!cardExpanded)
                            mBinding.cardContent.passwordConfirmTextLayout.setVisibility(View.GONE);
                    }

                    @Override
                    public void onAnimationCancel(Animator animation) {
                    }

                    @Override
                    public void onAnimationRepeat(Animator animation) {

                    }
                });
    }

    private void showMain() {
        AlphaAnimation anim = new AlphaAnimation(0f, 1f);
        anim.setDuration(350);
        anim.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {
                mBinding.textView3.setVisibility(View.VISIBLE);
                mBinding.textView4.setVisibility(View.VISIBLE);
            }

            @Override
            public void onAnimationEnd(Animation animation) {

            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
        mBinding.textView3.startAnimation(anim);
        mBinding.textView4.startAnimation(anim);
    }

    private void hideMain() {
        AlphaAnimation anim = new AlphaAnimation(1f, 0f);
        anim.setDuration(350);
        anim.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                mBinding.textView3.setVisibility(View.GONE);
                mBinding.textView4.setVisibility(View.GONE);
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
        mBinding.textView3.startAnimation(anim);
        mBinding.textView4.startAnimation(anim);

    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnSignInFragmentInteractionListener {
        void onSignInStarted();
        void onSignInCompleted(boolean success);
    }
}
