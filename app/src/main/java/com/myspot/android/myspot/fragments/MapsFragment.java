package com.myspot.android.myspot.fragments;

import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Build;
import android.os.Bundle;
import android.speech.RecognizerIntent;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.DatePicker;
import android.widget.TimePicker;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.places.Places;
import com.google.android.gms.location.places.ui.PlaceAutocomplete;
import com.mancj.materialsearchbar.MaterialSearchBar;
import com.mancj.materialsearchbar.adapter.SuggestionsAdapter;
import com.myspot.android.myspot.MSMap;
import com.myspot.android.myspot.MainActivity;
import com.myspot.android.myspot.R;
import com.myspot.android.myspot.databinding.FragmentMapsBinding;
import com.myspot.android.myspot.models.Listing;
import com.myspot.android.myspot.utils.Constants;
import com.myspot.android.myspot.utils.Util;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

import static android.Manifest.permission.ACCESS_FINE_LOCATION;
import static android.app.Activity.RESULT_OK;
import static android.support.v4.content.PermissionChecker.PERMISSION_GRANTED;
import static com.myspot.android.myspot.utils.Util.REQ_FINE_LOCATION;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link OnMapsFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link MapsFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class MapsFragment extends Fragment {
    private static final String TAG = MapsFragment.class.getSimpleName();
    private static final int RES_SPEECH_REQUEST = 3;

    private OnMapsFragmentInteractionListener mListener;

    private FragmentMapsBinding mBinding;

    private MSMap mMSMap;

    private Calendar mDateFromSelected, mDateUntilSelected;

    private Date dateFrom, dateUntil;
    private Listing mSelectedListing;
    private String mSelectedListingId;
    private GoogleApiClient mApiClient;

    public MapsFragment() {}

    public static MapsFragment newInstance(Date dateFrom, Date dateUntil) {
        MapsFragment fragment = new MapsFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        fragment.setDateFrom(dateFrom);
        fragment.setDateUntil(dateUntil);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mMSMap = new MSMap();
        mMSMap.bindFragment(this);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_maps, container, false);
        mMSMap.bindFragmentBinding(mBinding);
        mMSMap.setupCallbacksAndListeners();
        mMSMap.setupMapFragment();
        mApiClient = new GoogleApiClient
                .Builder(getActivity())
                .addApi(Places.GEO_DATA_API)
                .addApi(Places.PLACE_DETECTION_API)
                .enableAutoManage(getActivity(), new GoogleApiClient.OnConnectionFailedListener() {
                    @Override
                    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {Log.e(TAG, "onConnectionFailed: " + connectionResult.getErrorMessage());
                    }
                }).build();

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            checkLocationPermission();
        }

        mBinding.searchBar.setMapMode();
        mBinding.dateFrom.setText(Util.rangeDateFormat.format(new Date()));
        mBinding.timeFrom.setText(Util.rangeTimeFormat.format(new Date()));
        mBinding.dateUntil.setText(Util.defaultUntilDate());
        mBinding.timeUntil.setText(Util.defaultUntilTime());
        mBinding.dateUntil.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {onDateUntilClicked();}
        });
        mBinding.timeUntil.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {onTimeUntilClicked();}
        });
        mBinding.dateFrom.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {onDateFromClicked();}});
        mBinding.timeFrom.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {onTimeFromClicked();}});
        mBinding.searchBar.setHint(getString(R.string.hint_map));
        mBinding.searchBar.setSpeechMode(true);

        mBinding.searchBar.setOnSearchActionListener(new MaterialSearchBar.OnSearchActionListener()  {
            @Override
            public void onSearchStateChanged(boolean enabled) {
                Log.d(TAG, "onSearchStateChanged: enabled ? " + enabled);
                if (enabled) {
                    try {
                        Intent intent = new PlaceAutocomplete.IntentBuilder(PlaceAutocomplete.MODE_FULLSCREEN)
                                .build(getActivity());
                        startActivityForResult(intent, Constants.REQUEST_PLACE_SUGGESTION);
                    } catch (Exception e) {
                        Log.e(TAG, "onFocusChange: " + Log.getStackTraceString(e));
                    }
                }
            }

            @Override
            public void onSearchConfirmed(CharSequence text) {
                Log.d(TAG, "onSearchConfirmed:");
                if (mMSMap != null) {
                    mMSMap.onMapSearch(text.toString());
                }
            }

            @Override
            public void onButtonClicked(int buttonCode) {
                Log.d(TAG, "onButtonClicked:");
                mBinding.searchBar.requestFocus();
                mBinding.searchBar.enableSearch();
                Intent intent = new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);
                intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL, RecognizerIntent.LANGUAGE_MODEL_FREE_FORM);
                startActivityForResult(intent, RES_SPEECH_REQUEST);
            }
        });
        mBinding.searchBar.addTextChangeListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {}

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                Log.d(TAG, "onTextChanged: " + s.toString());
                if (!TextUtils.isEmpty(s)) {
                    mBinding.searchBar.getLocationSuggestions(s.toString(), mApiClient,
                            mMSMap.getCurrentLatLng());

                } else {
                    mBinding.searchBar.clearSuggestions();
                }
            }

            @Override
            public void afterTextChanged(Editable s) {}
        });
        mBinding.searchBar.setSuggstionsClickListener(new SuggestionsAdapter.OnItemViewClickListener() {
            @Override
            public void OnItemClickListener(int position, View v) {
                if (position < mBinding.searchBar.getLastSuggestions().size()) {
                    String suggestionText = (String) mBinding.searchBar.getLastSuggestions().get(position);
                    mBinding.searchBar.setText(suggestionText);
                    if (mMSMap != null) {
                        mMSMap.onMapSearch(suggestionText);
                    }
                    List<String> newList = mBinding.searchBar.getLastSuggestions();
                    newList.remove(position);
                    mBinding.searchBar.updateLastSuggestions(newList);
                } else {
                    Log.w(TAG, "OnItemClickListener: tried to get a suggestion that's out of bounts!");
                }
            }

            @Override
            public void OnItemDeleteListener(int position, View v) { }
        });

        mDateUntilSelected = Calendar.getInstance();
        mDateFromSelected = Calendar.getInstance();

        mDateUntilSelected.setTime(((MainActivity) getActivity()).getSavedData().getDateUntil());
        mDateFromSelected.setTime(((MainActivity) getActivity()).getSavedData().getDateFrom());

        return mBinding.mapLayout;
    }

    private void onTimeFromClicked() {
        mDateFromSelected = Calendar.getInstance();
        mDateFromSelected.setTime(dateFrom);
        new TimePickerDialog(getContext(), new TimePickerDialog.OnTimeSetListener() {
            @Override
            public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                mDateFromSelected.set(Calendar.HOUR_OF_DAY, hourOfDay);
                mDateFromSelected.set(Calendar.MINUTE, minute);
                Calendar today = Calendar.getInstance();
                today.setTime(new Date());
                if (mDateFromSelected.after(today)) {
                    dateFrom = mDateFromSelected.getTime();
                    mBinding.timeFrom.setText(Util.rangeTimeFormat.format(dateFrom));
                    if (dateUntil.before(dateFrom)) {
                        Log.d(TAG, "onTimeSet: dateUntil before dateFrom!");
                        mDateUntilSelected.setTime(dateFrom);
                        mDateUntilSelected.add(Calendar.DAY_OF_MONTH, 1);
                        dateUntil = mDateUntilSelected.getTime();
                        mBinding.dateUntil.setText(Util.rangeDateFormat.format(dateUntil));
                    }
                } else {
                    Toast.makeText(getContext(), R.string.err_time_before_today, Toast.LENGTH_LONG).show();
                    //Snackbar.make(mBinding.mapLayout, R.string.err_time_before_today, Snackbar.LENGTH_SHORT).show();
                }
            }
        }, mDateFromSelected.get(Calendar.HOUR_OF_DAY), mDateFromSelected.get(Calendar.MINUTE), false)
                .show();
    }

    private void onDateFromClicked() {
        mDateFromSelected = Calendar.getInstance();
        mDateFromSelected.setTime(dateFrom);
        new DatePickerDialog(getContext(), new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                mDateFromSelected.set(Calendar.YEAR, year);
                mDateFromSelected.set(Calendar.MONTH, month);
                mDateFromSelected.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                Calendar today = Calendar.getInstance();
                today.setTime(new Date());
                if (mDateFromSelected.after(today)) {
                    dateFrom = mDateFromSelected.getTime();
                    mBinding.dateFrom.setText(Util.rangeDateFormat.format(dateFrom));
                    if (dateUntil.before(dateFrom)) {
                        mDateUntilSelected.setTime(dateFrom);
                        mDateUntilSelected.add(Calendar.DATE, 1);
                        dateUntil = mDateFromSelected.getTime();
                        mBinding.dateUntil.setText(Util.rangeDateFormat.format(dateUntil));
                    }
                } else {
                    Toast.makeText(getContext(), R.string.err_time_before_today, Toast.LENGTH_LONG).show();
                    //Snackbar.make(mBinding.mapLayout, R.string.err_time_before_today, Snackbar.LENGTH_SHORT).show();
                }
            }
        }, mDateFromSelected.get(Calendar.YEAR), mDateFromSelected.get(Calendar.MONTH), mDateFromSelected.get(Calendar.DAY_OF_MONTH))
                .show();
    }

    private void onTimeUntilClicked() {
        mDateUntilSelected = Calendar.getInstance();
        mDateUntilSelected.setTime(dateFrom);
        new TimePickerDialog(getContext(), new TimePickerDialog.OnTimeSetListener() {
            @Override
            public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                mDateUntilSelected.set(Calendar.HOUR_OF_DAY, hourOfDay);
                mDateUntilSelected.set(Calendar.MINUTE, minute);
                Calendar today = Calendar.getInstance();
                today.setTime(new Date());
                if (mDateUntilSelected.after(today)) {
                    Calendar fromC = Calendar.getInstance();
                    fromC.setTime(dateFrom);
                    if (mDateUntilSelected.before(fromC)) {
                        Toast.makeText(getContext(), R.string.err_time_before_from, Toast.LENGTH_LONG).show();
                        //Snackbar.make(mBinding.mapLayout, R.string.err_time_before_from, Snackbar.LENGTH_SHORT).show();
                    } else {
                        dateUntil = mDateUntilSelected.getTime();
                        mBinding.timeUntil.setText(Util.rangeTimeFormat.format(dateUntil));
                    }
                } else {
                    Toast.makeText(getContext(), R.string.err_time_before_today, Toast.LENGTH_LONG).show();
                    //Snackbar.make(mBinding.mapLayout, R.string.err_time_before_today, Snackbar.LENGTH_SHORT).show();
                }
            }
        }, mDateUntilSelected.get(Calendar.HOUR_OF_DAY), mDateUntilSelected.get(Calendar.MINUTE), false) // TODO: 5/18/2017 Set 24hr mode based on location
                .show();
    }

    private void onDateUntilClicked() {
        mDateUntilSelected = Calendar.getInstance();
        mDateUntilSelected.setTime(dateUntil);
        DatePickerDialog datePickerDialog = new DatePickerDialog(getContext(), new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                mDateUntilSelected.set(Calendar.YEAR, year);
                mDateUntilSelected.set(Calendar.MONTH, month);
                mDateUntilSelected.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                Calendar today = Calendar.getInstance();
                today.setTime(new Date());
                if (mDateUntilSelected.after(today)) {
                    Calendar fromC = Calendar.getInstance();
                    fromC.setTime(dateFrom);
                    if (mDateUntilSelected.before(fromC)) {
                        Toast.makeText(getContext(), R.string.err_time_before_from, Toast.LENGTH_LONG).show();
                        //Snackbar.make(mBinding.mapLayout, R.string.err_time_before_from, Snackbar.LENGTH_SHORT).show();
                    } else {
                        dateUntil = mDateUntilSelected.getTime();
                        mBinding.dateUntil.setText(Util.rangeDateFormat.format(dateUntil));
                    }
                } else {
                    Toast.makeText(getContext(), R.string.err_date_before_today, Toast.LENGTH_LONG).show();
                    //Snackbar.make(mBinding.mapLayout, R.string.err_date_before_today, Snackbar.LENGTH_SHORT).show();
                }
            }
        }, mDateUntilSelected.get(Calendar.YEAR), mDateUntilSelected.get(Calendar.MONTH), mDateUntilSelected.get(Calendar.DAY_OF_MONTH));
        datePickerDialog.show();
    }

    // TODO: 2/23/2017 change this to fragment version so hacky workaround is not needed
    private boolean checkLocationPermission() {
        if (ContextCompat.checkSelfPermission(getContext(), ACCESS_FINE_LOCATION) != PERMISSION_GRANTED) {
            // Asking user if explanation is needed
            if (ActivityCompat.shouldShowRequestPermissionRationale(getActivity(), ACCESS_FINE_LOCATION)) {
                ActivityCompat.requestPermissions(getActivity(), new String[]{ACCESS_FINE_LOCATION}, REQ_FINE_LOCATION);
            } else {
                // No explanation needed, we can request the permission.
                ActivityCompat.requestPermissions(getActivity(), new String[]{ACCESS_FINE_LOCATION}, REQ_FINE_LOCATION);
            }
            return false;
        } else {
            return true;
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        Log.d(TAG, "onRequestPermissionsResult: ran");
        switch (requestCode) {
            case REQ_FINE_LOCATION: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0 && grantResults[0] == PERMISSION_GRANTED) {
                    if (checkLocationPermission()) {
                        Log.d(TAG, "onRequestPermissionsResult: Permission Granted");
                        mMSMap.readyMap();
                    }
                } else {
                    Snackbar.make(mBinding.mapLayout, R.string.need_fine_access,
                            Snackbar.LENGTH_INDEFINITE)
                            .setAction(android.R.string.ok, new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                }
                            }).show();
                }
            }
            break;
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == RES_SPEECH_REQUEST) {
            if (resultCode == RESULT_OK) {
                mBinding.searchBar
                        .setText(data.getStringArrayListExtra(RecognizerIntent.EXTRA_RESULTS).get(0));
            }
        } else if (requestCode == Constants.REQUEST_PLACE_SUGGESTION) {
            if (resultCode == RESULT_OK) {
                mBinding.searchBar.setText(PlaceAutocomplete.getPlace(getContext(), data).getAddress().toString());
            }
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void onStart() {
        super.onStart();
        mMSMap.onStart();
    }

    @Override
    public void onStop() {
        super.onStop();
        mMSMap.onStop();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnMapsFragmentInteractionListener) {
            mListener = (OnMapsFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnMapsFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onDestroy() {
        Log.d(TAG, "onDestroy: ");
        mMSMap.onDestroy();
        mMSMap = null;
        super.onDestroy();
    }

    public OnMapsFragmentInteractionListener getListener() {
        return mListener;
    }


    public void setDateFrom(Date dateFrom) {
        this.dateFrom = dateFrom;
    }

    public void setDateUntil(Date dateUntil) {
        this.dateUntil = dateUntil;
    }

    public boolean onBackPressed() {
        if (mBinding.searchBar.isActivated()) {
            mBinding.searchBar.disableSearch();
            return true;
        } else {
            return false;
        }
    }

    public void showListingDetails(Listing newListing, String listingId) {
        mSelectedListing = newListing;
        mSelectedListingId = listingId;
        MainActivity mainActivity = (MainActivity) getActivity();
        if (mainActivity != null) {
            mainActivity.showListingDetails(newListing, listingId);
        }
    }

    public Listing getSelectedListing() {
        return mSelectedListing;
    }

    public String getSelectedListingId() {
        return mSelectedListingId;
    }

    public interface OnMapsFragmentInteractionListener {
        void onMapReady();
    }
}
