package com.myspot.android.myspot.utils;


public class Constants {
    /*
     * Preference Tags
     */
    public static final String USER_PREFS = "UserPrefs";
    public static final String USER_PREFS_PROFILE_URL = "ProfileUrl";
    public static final String USER_PREFS_EMAIL = "UserEmail";
    public static final String USER_PREFS_NAME = "UserPrefsName";
    public static final String USER_PREFS_UID = "UserPrefsUID";
    public static final String USER_PREFS_MAP_TYPE = "UserPrefsMapType";
    public static final String SIGNED_IN = "IS_SIGNED_IN";

    /*
     * Activity Result Request Codes
     */
    public static final int REQUEST_GOOGLE_SIGN_IN = 1;
    public static final int REQUEST_FACEBOOK_SIGN_IN = 2;
    public static final int REQUEST_PLACE_PICK = 3;
    public static final int REQUEST_IMAGE_PICK = 4;
    public static final int REQUEST_PLACE_SUGGESTION = 5;
}
