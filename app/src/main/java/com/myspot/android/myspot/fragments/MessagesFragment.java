package com.myspot.android.myspot.fragments;

import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.firebase.ui.database.FirebaseIndexRecyclerAdapter;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.FirebaseDatabase;
import com.myspot.android.myspot.MessageActivity;
import com.myspot.android.myspot.R;
import com.myspot.android.myspot.databinding.FragmentMessagesBinding;
import com.myspot.android.myspot.models.Conversation;
import com.myspot.android.myspot.view_holders.ConversationHolder;

import jp.wasabeef.recyclerview.adapters.SlideInBottomAnimationAdapter;

import static com.myspot.android.myspot.MessageActivity.EXTRA_CIRCULAR_REVEAL_X;
import static com.myspot.android.myspot.MessageActivity.EXTRA_CIRCULAR_REVEAL_Y;
import static com.myspot.android.myspot.MessageActivity.EXTRA_CONVERSATION;
import static com.myspot.android.myspot.MessageActivity.EXTRA_CONV_ID;
import static com.myspot.android.myspot.MessageActivity.EXTRA_OTHER_NAME;

public class MessagesFragment extends Fragment {

    private FragmentMessagesBinding mBinding;
    private FirebaseIndexRecyclerAdapter mConversationAdapter = new FirebaseIndexRecyclerAdapter<Conversation, ConversationHolder>(
            Conversation.class,
            R.layout.content_conversation,
            ConversationHolder.class,
            FirebaseDatabase.getInstance().getReference("users")
                    .child(FirebaseAuth.getInstance().getCurrentUser().getUid())
                    .child("conversations"),
            FirebaseDatabase.getInstance().getReference("conversations")) {

        @Override
        protected void populateViewHolder(ConversationHolder viewHolder, Conversation model, int position) {
            viewHolder.bind(model, FirebaseAuth.getInstance().getCurrentUser().getUid(), getRef(position).getKey(), mListener, getActivity());
        }

        @Override
        public void onViewRecycled(ConversationHolder holder) {
            holder.cleanup();
            super.onViewRecycled(holder);
        }
    };

    private OnConversationClickedListener mListener = new OnConversationClickedListener() {
        @Override
        public void onClicked(View profileImageView, Conversation conversation, String key, String name) {presentMessageActivity(profileImageView, conversation, key, name);
        }};

    public MessagesFragment() {}

    public static MessagesFragment newInstance() {
        MessagesFragment fragment = new MessagesFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onDestroy() {
        mConversationAdapter.cleanup();
        super.onDestroy();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        mBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_messages, container, false);
        mBinding.messageList.setHasFixedSize(false);
        mBinding.messageList.setLayoutManager(new LinearLayoutManager(getContext()));
        mBinding.messageList.setAdapter(new SlideInBottomAnimationAdapter(mConversationAdapter));
        return mBinding.mainView;
    }

    public void presentMessageActivity(View v, Conversation conversation, String key, String name) {
        Intent intent = new Intent(v.getContext(), MessageActivity.class);
        int revealX = (int) (v.getX() + v.getWidth() / 2);
        int revealY = (int) (v.getY() + v.getHeight() / 2);

        ActivityOptionsCompat options = ActivityOptionsCompat
                .makeClipRevealAnimation(v, revealX, revealY, v.getWidth(), v.getHeight());

        intent.putExtra(EXTRA_CONVERSATION, conversation);
        intent.putExtra(EXTRA_CONV_ID, key);
        intent.putExtra(EXTRA_OTHER_NAME, name);
        intent.putExtra(EXTRA_CIRCULAR_REVEAL_X, revealX);
        intent.putExtra(EXTRA_CIRCULAR_REVEAL_Y, revealY);

        ActivityCompat.startActivity(getContext(), intent, options.toBundle());
    }

    public interface OnConversationClickedListener {
        void onClicked(View profileImageView, Conversation conversation, String key, String name);
    }
}
