package com.myspot.android.myspot;

import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.util.Log;

import com.firebase.geofire.GeoFire;
import com.firebase.geofire.GeoLocation;
import com.firebase.geofire.GeoQuery;
import com.firebase.geofire.GeoQueryEventListener;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.myspot.android.myspot.databases.MapDatabase;
import com.myspot.android.myspot.databinding.FragmentMapsBinding;
import com.myspot.android.myspot.fragments.MapsFragment;
import com.myspot.android.myspot.models.Listing;
import com.myspot.android.myspot.models.Spot;
import com.myspot.android.myspot.models.SpotMarker;

import java.util.List;

import static android.Manifest.permission.ACCESS_FINE_LOCATION;
import static android.support.v4.content.PermissionChecker.PERMISSION_GRANTED;

public class MSMap {
    private static final String TAG = MSMap.class.getSimpleName();

    private FragmentMapsBinding mBinding;
    private MapsFragment mMapsFragment;

    /**
     * Auth object used to ensure firebase user is logged in
     */
    private FirebaseAuth mAuth;

    /**
     * Listener used to notify that firebase user is logged in
     */
    private FirebaseAuth.AuthStateListener mAuthListener;

    /**
     * Used to get spots within certain area
     */
    private GeoQueryEventListener mGeoQueryEventListener;

    /**
     * Callback used to finish setting up GoogleMap and bind ready map object
     */
    private OnMapReadyCallback mOnMapReadyCallback;

    /**
     * Listens for when the user changes location and updates the map accordingly
     */
    private LocationListener mLocationListener;

    /**
     * Handles and sends a callback to the fragment when a specific marker is selected
     */
    private GoogleMap.OnMarkerClickListener mMarkerListener;

    /**
     * The GoogleMap object
     */
    private GoogleMap mMap;
    private GoogleApiClient mGoogleApiClient;

    /**
     * Stores the Location recieved from the latest update from mLocationListener
     */
    private Location mLastLocation;

    /**
     * GeoQuery object used to get spots in a certain location
     */
    private GeoQuery mGeoQuery;

    /**
     * Stores the key of the selected spot on the map, is null if no spot is selected
     */
    private String mSelectedSpotKey;
    private LatLng mCurrentLatLng;


    public void onMapSearch(String location) {
        if (location != null && !location.equals("")) {
            Geocoder geocoder = new Geocoder(mMapsFragment.getContext());
            List<Address> addressList;

            try {
                addressList = geocoder.getFromLocationName(location, 1);
                Address address = addressList.get(0);
                LatLng latLng = new LatLng(address.getLatitude(), address.getLongitude());
                mCurrentLatLng = latLng;
                mMap.animateCamera(CameraUpdateFactory.newLatLng(latLng));
            } catch (Exception e) {
                Log.e(TAG, "onMapSearch: " + Log.getStackTraceString(e));
            }
        }
    }

    public void bindFragment(MapsFragment mapsFragment) {
        mMapsFragment = mapsFragment;
    }
    public void bindFragmentBinding(FragmentMapsBinding binding) {
        mBinding = binding;
    }

    public void setupCallbacksAndListeners() {
        if (mMapsFragment != null && mBinding != null) {
            mAuth = FirebaseAuth.getInstance();
            mAuthListener = new FirebaseAuth.AuthStateListener() {
                @Override
                public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
                    Log.d(TAG, "onAuthStateChanged: currently signed in ? " + (firebaseAuth.getCurrentUser() == null));
                    FirebaseUser user = firebaseAuth.getCurrentUser();
                    if (user == null) {
                        mAuth.signInAnonymously().addOnCompleteListener(mMapsFragment.getActivity(), new OnCompleteListener<AuthResult>() {
                            @Override
                            public void onComplete(@NonNull Task<AuthResult> task) {
                                setupGeoQuery();
                            }
                        });
                    } else {
                        setupGeoQuery();
                    }
                }
            };
            mOnMapReadyCallback = new OnMapReadyCallback() {
                @Override
                public void onMapReady(GoogleMap googleMap) {
                    Log.d(TAG, "onMapReady: Ready!");
                    mMap = googleMap;
                    mMap.setMapType(GoogleMap.MAP_TYPE_HYBRID);

                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                        if (ContextCompat.checkSelfPermission(mMapsFragment.getContext(),
                                ACCESS_FINE_LOCATION) == PERMISSION_GRANTED) {
                            readyMap();
                        }
                    } else {
                        readyMap();
                    }
                }
            };
            mLocationListener = new LocationListener() {
                @Override
                public void onLocationChanged(Location location) {
                    Log.d(TAG, "onLocationChanged: location changed!");
                    mLastLocation = location;

                    if (mAuth.getCurrentUser() != null) {
                        setupGeoQuery();
                    }

                    //move mDataMap camera
                    mMap.moveCamera(CameraUpdateFactory.newLatLng(new LatLng(location.getLatitude(), location.getLongitude())));
                    mMap.animateCamera(CameraUpdateFactory.zoomTo(16));

                    //stop location updates
                    if (mGoogleApiClient != null) {
                        LocationServices.FusedLocationApi.removeLocationUpdates(mGoogleApiClient, this);
                    }
                }
            };
            mMarkerListener = new GoogleMap.OnMarkerClickListener() {
                @Override
                public boolean onMarkerClick(final Marker marker) {
                    Listing listing = MapDatabase.MapListings.getListingFromMarker(marker.getId());
                    Log.d(TAG, "onMarkerClick: showing Listing...");
                    if (listing != null) {
                        Log.d(TAG, "onMarkerClick: listing is not null!");
                        if (mMapsFragment != null) {
                            Log.d(TAG, "onMarkerClick: frag not null!" );

                            mMapsFragment.showListingDetails(listing, MapDatabase.MapListings.getMarkerByMarkerId(marker.getId()).getListingId());
                        } else {
                            Log.e(TAG, "onMarkerClick: mMapsFragment was NULL" );
                            // TODO: 6/1/2017 cache for later
                        }
                    } else {
                        Log.d(TAG, "onMarkerClick: fetching listing...");
                        SpotMarker spotMarker = MapDatabase.MapListings.getMarkerByMarkerId(marker.getId());
                        if (spotMarker != null) {
                            Log.d(TAG, "onMarkerClick: trying to fetch listing " + spotMarker.getListingId());
                            FirebaseDatabase.getInstance().getReference("listings")
                                    .child(spotMarker.getListingId())
                                    .addListenerForSingleValueEvent(new ValueEventListener() {
                                        @Override
                                        public void onDataChange(DataSnapshot dataSnapshot) {
                                            Listing newListing = dataSnapshot.getValue(Listing.class);
                                            SpotMarker spotMarker1 = MapDatabase.MapListings.getMarkerByMarkerId(marker.getId());
                                            if (newListing != null && mMapsFragment != null && spotMarker1 != null) {
                                                Log.d(TAG, "onDataChange: showing new listing!");
                                                mMapsFragment.showListingDetails(newListing, spotMarker1.getListingId());
                                                MapDatabase.MapListings.putListing(newListing, dataSnapshot.getKey());
                                            } else {
                                                Log.e(TAG, "onDataChange: newListing was null for " + dataSnapshot.getKey());
                                            }
                                        }

                                        @Override
                                        public void onCancelled(DatabaseError databaseError) {}
                                    });

                        } else {
                            Log.e(TAG, "onMarkerClick: spotMarker was null for marker id " + marker.getId());
                        }
                    }

                    return false;
                }
            };
            mGeoQueryEventListener = new GeoQueryEventListener() {
                @Override
                public void onKeyEntered(String key, GeoLocation location) {
                    Log.d(TAG, "onKeyEntered: " + key);
                    Marker marker = mMap.addMarker(new MarkerOptions()
                            .position(new LatLng(location.latitude, location.longitude)));
                    MapDatabase.MapListings.putMarker(new SpotMarker(marker, key));
                    FirebaseDatabase.getInstance().getReference("listings").child(key)
                            .addListenerForSingleValueEvent(new ValueEventListener() {
                                @Override
                                public void onDataChange(DataSnapshot dataSnapshot) {
                                    Listing listing = dataSnapshot.getValue(Listing.class);
                                    if (listing != null) {
                                        MapDatabase.MapListings.putListing(listing, dataSnapshot.getKey());
                                    } else {
                                        Log.e(TAG, "onDataChange: null object for "  + dataSnapshot.getKey());
                                    }
                                }

                                @Override
                                public void onCancelled(DatabaseError databaseError) {
                                    if (databaseError != null) {Log.e(TAG, "onCancelled: " + Log.getStackTraceString(databaseError.toException()));}
                                }
                            });
                }

                @Override
                public void onKeyExited(String key) {
                    Log.d(TAG, "onKeyExited: " + key);
                    Marker marker = MapDatabase.MapListings.removeMarkerByListingKey(key);
                    if (marker != null) {
                        marker.remove();
                    } else {
                        Log.w(TAG, "onKeyExited: fetched marker was null! (" + key + ")");
                    }
                }

                @Override
                public void onKeyMoved(String key, GeoLocation location) {Log.d(TAG, "onKeyMoved: " + key);}

                @Override
                public void onGeoQueryReady() {Log.d(TAG, "onGeoQueryReady: ready!");}

                @Override
                public void onGeoQueryError(DatabaseError error) {Log.e(TAG, "onGeoQueryError: " + Log.getStackTraceString(error.toException()));}
            };

            mAuthListener = new FirebaseAuth.AuthStateListener() {
                @Override
                public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
                    Log.d(TAG, "onAuthStateChanged: currently signed in ? " + (firebaseAuth.getCurrentUser() == null));
                    FirebaseUser user = firebaseAuth.getCurrentUser();
                    if (user == null) {
                        mAuth.signInAnonymously().addOnCompleteListener(mMapsFragment.getActivity(), new OnCompleteListener<AuthResult>() {
                            @Override
                            public void onComplete(@NonNull Task<AuthResult> task) {
                                setupGeoQuery();
                            }
                        });
                    } else {
                        setupGeoQuery();
                    }
                }
            };
        } else {
            Log.e(TAG, "setupCallbacksAndListeners: Can't setup callbacks and listeners before binding MSMap!");
        }
    }

    private void setupGeoQuery() {
        if (mMap != null && mAuth.getCurrentUser() != null && mLastLocation != null) {
            Log.d(TAG, "setupGeoQuery: called and ready!");
            DatabaseReference ref = FirebaseDatabase.getInstance().getReference("geofire");
            GeoFire geoFire = new GeoFire(ref);
            mGeoQuery = geoFire.queryAtLocation(new GeoLocation(mLastLocation.getLatitude(), mLastLocation.getLongitude()), 2);
            mGeoQuery.addGeoQueryEventListener(mGeoQueryEventListener);
        } else {
            Log.w(TAG, "setupGeoQuery: called but not ready!");
        }
        Log.d(TAG, "setupGeoQuery: done!");
    }

    protected synchronized void buildGoogleApiClient() {
        mGoogleApiClient = new GoogleApiClient.Builder(mMapsFragment.getContext())
                .addConnectionCallbacks(new GoogleApiClient.ConnectionCallbacks() {
                    @Override
                    public void onConnected(@Nullable Bundle bundle) {
                        LocationRequest mLocationRequest = new LocationRequest();
                        mLocationRequest.setInterval(1000);
                        mLocationRequest.setFastestInterval(1000);
                        mLocationRequest.setPriority(LocationRequest.PRIORITY_BALANCED_POWER_ACCURACY);
                        if (ContextCompat.checkSelfPermission(mMapsFragment.getContext(),
                                ACCESS_FINE_LOCATION) == PERMISSION_GRANTED) {
                            LocationServices.FusedLocationApi
                                    .requestLocationUpdates(mGoogleApiClient, mLocationRequest, mLocationListener);
                        }
                    }

                    @Override
                    public void onConnectionSuspended(int i) {}
                })
                .addOnConnectionFailedListener(new GoogleApiClient.OnConnectionFailedListener() {
                    @Override
                    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {}
                })
                .addApi(LocationServices.API)
                .build();
        mGoogleApiClient.connect();
    }

    @SuppressWarnings("MissingPermission")
    public void readyMap() {
        buildGoogleApiClient();
        mMap.setMyLocationEnabled(true);
        mMap.setOnMarkerClickListener(mMarkerListener);
        if (mMapsFragment != null) {
            mMapsFragment.getListener().onMapReady();
        }
        // TODO: 2/14/2017 add any saved listings
    }

    public void setupMapFragment() {
        SupportMapFragment mapFragment = SupportMapFragment.newInstance();
        mMapsFragment.getActivity().getSupportFragmentManager().beginTransaction()
                .replace(R.id.map_frag, mapFragment).commit();
        mapFragment.getMapAsync(mOnMapReadyCallback);
    }

    public void onDestroy() {
        mMapsFragment = null;
        mBinding = null;
    }

    public void onStop() {
        mAuth.removeAuthStateListener(mAuthListener);
    }

    public void onStart() {
        mAuth.addAuthStateListener(mAuthListener);
    }

    public LatLng getCurrentLatLng() {
        if (mLastLocation != null) {
            return new LatLng(mLastLocation.getLatitude(), mLastLocation.getLongitude());
        } else {
            return new LatLng(24,24);
        }
    }
}
