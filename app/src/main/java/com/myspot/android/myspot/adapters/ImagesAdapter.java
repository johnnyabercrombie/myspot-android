package com.myspot.android.myspot.adapters;

import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.esafirm.imagepicker.features.ImagePicker;
import com.esafirm.imagepicker.model.Image;
import com.myspot.android.myspot.R;

import java.util.ArrayList;
import java.util.List;

import static com.myspot.android.myspot.utils.Constants.REQUEST_IMAGE_PICK;

public class ImagesAdapter  extends RecyclerView.Adapter<ImagesAdapter.ImageViewHolder> {
    private static final String TAG = ImagesAdapter.class.getSimpleName();
    private static final int IMAGE = 1;
    private static final int ADD_IMAGE = 0;

    private List<Image> imageList;

    private Fragment context;

    public ImagesAdapter(Fragment activity) {
        imageList = new ArrayList<>();
        this.context = activity;
    }

    @Override
    public int getItemViewType(int position) {
        return position == 0 ? ADD_IMAGE : IMAGE;
    }

    @Override
    public ImageViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return viewType == ADD_IMAGE
                ? new ImageViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_listing_add_photo, parent, false))
                : new ImageViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_listing_image, parent, false));
    }

    @Override
    public void onBindViewHolder(final ImageViewHolder holder, int position) {
        if (position == 0) {
            Log.d(TAG, "onBindViewHolder: binding ADD IMAGE");
            holder.bind(false, null, new OnClickListener() {
                @Override
                public void onClicked() {
                    Log.d(TAG, "onClicked: addImage clicked!");
                    if (context != null) {
                        Log.d(TAG, "onClicked: getting images...");
                        ImagePicker.create(context).multi().limit(10).start(REQUEST_IMAGE_PICK);
                    } else {
                        Log.d(TAG, "onClicked: context was null!");
                    }
                }

                @Override
                public boolean onLongClicked() {
                    return false;
                }
            });
        } else {
            Log.d(TAG, "onBindViewHolder: binding IMAGE");
            holder.bind(true, imageList.get(position - 1), new OnClickListener() {
                @Override
                public void onClicked() {
                    Log.d(TAG, "onClicked: Image clicked!");
                    // TODO: 5/22/2017 show enlarged image
                }

                @Override
                public boolean onLongClicked() {
                    Log.d(TAG, "onLongClicked: image long clicked!");
                    holder.showRemoveViews();
                    holder.itemView.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            imageList.remove(holder.getAdapterPosition());
                            notifyItemRemoved(holder.getAdapterPosition());
                        }
                    });
                    return true;
                }
            });
        }
    }

    public List<Image> getImageList() {
        return imageList;
    }

    public void cleanup() {
        this.context = null;
    }

    public void setContext(Fragment activity) {
        this.context = activity;
    }

    @Override
    public void onViewRecycled(ImageViewHolder holder) {
        Log.d(TAG, "onViewRecycled: ");
        holder.cleanup();
        super.onViewRecycled(holder);
    }

    @Override
    public int getItemCount() {
        return imageList.size() + 1;
    }

    public void addImages(List<Image> images) {
        for (Image image : images) {
            if (!imageList.contains(image)) {
                imageList.add(image);
                notifyItemInserted(imageList.size()-1);
            }
        }
    }

    static class ImageViewHolder extends RecyclerView.ViewHolder {
        private Boolean isImageView;
        private OnClickListener listener;

        ImageViewHolder(View itemView) {
            super(itemView);
            isImageView = false;
        }

        public void bind(boolean isImageView, @Nullable Image drawable, OnClickListener listener) {
            this.isImageView = isImageView;
            this.listener = listener;
            if (isImageView && drawable != null) {
                ImageView imageView = (ImageView) itemView.findViewById(R.id.image_view);
                Glide.with(imageView.getContext()).load(drawable.getPath()).crossFade(350).into(imageView);
                itemView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {ImageViewHolder.this.listener.onClicked();
                    }
                });
                itemView.setOnLongClickListener(new View.OnLongClickListener() {
                    @Override
                    public boolean onLongClick(View v) {return ImageViewHolder.this.listener.onLongClicked();}});
            } else {
                View view = itemView.findViewById(R.id.add_photo_button);
                view.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {ImageViewHolder.this.listener.onClicked();
                    }
                });
                view.setOnLongClickListener(new View.OnLongClickListener() {
                    @Override
                    public boolean onLongClick(View v) {return ImageViewHolder.this.listener.onLongClicked();}});
            }
        }

        void cleanup() {
            if (isImageView) {
                ImageView imageView = (ImageView) itemView.findViewById(R.id.image_view);
                View view = itemView.findViewById(R.id.main_view);
                view.findViewById(R.id.remove_scrim).setVisibility(View.GONE);
                view.findViewById(R.id.remove_text).setVisibility(View.GONE);
                view.findViewById(R.id.remove_x).setVisibility(View.GONE);
                imageView.setImageDrawable(null);
                view.setOnLongClickListener(null);
                view.setOnClickListener(null);
            }
            listener = null;
        }

        public void showRemoveViews() {
            if (isImageView) {
                itemView.findViewById(R.id.remove_scrim).setVisibility(View.VISIBLE);
                itemView.findViewById(R.id.remove_text).setVisibility(View.VISIBLE);
                itemView.findViewById(R.id.remove_x).setVisibility(View.VISIBLE);
            } else {
                Log.d(TAG, "showRemoveViews: not image view");
            }
        }
    }

    private interface OnClickListener {
        void onClicked();
        boolean onLongClicked();
    }
}
