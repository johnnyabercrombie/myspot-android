package com.myspot.android.myspot.view_holders;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.graphics.Typeface;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;

import com.bumptech.glide.Glide;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FileDownloadTask;
import com.myspot.android.myspot.MessageActivity;
import com.myspot.android.myspot.R;
import com.myspot.android.myspot.databases.ProfileImageDatabase;
import com.myspot.android.myspot.databinding.ContentConversationBinding;
import com.myspot.android.myspot.fragments.MessagesFragment;
import com.myspot.android.myspot.models.Conversation;
import com.myspot.android.myspot.models.User;
import com.myspot.android.myspot.utils.Util;

import java.io.File;

import static com.myspot.android.myspot.MessageActivity.EXTRA_CONVERSATION;
import static com.myspot.android.myspot.MessageActivity.EXTRA_CONV_ID;
import static com.myspot.android.myspot.MessageActivity.EXTRA_OTHER_NAME;

public class ConversationHolder extends RecyclerView.ViewHolder {
    private static final String TAG = ConversationHolder.class.getSimpleName();

    private ContentConversationBinding mBinding;
    private boolean imageSet;
    private Util.OnProfileSuccessListener mProfileSuccessListener = new Util.OnProfileSuccessListener() {
        @Override
        public void onSuccess(FileDownloadTask.TaskSnapshot taskSnapshot) {
            if (getuID() != null) {ProfileImageDatabase.addProfileImage(getuID(), getLocalFile());}
            setProfileImage(getLocalFile());
            cleanUp();
        }
    };

    private ValueEventListener mNameListener = new ValueEventListener() {
        @Override
        public void onDataChange(DataSnapshot dataSnapshot) {
            User.Info info = dataSnapshot.getValue(User.Info.class);
            name = info.getFirstName() + " " + info.getLastName();
            if (mBinding != null) {
                mBinding.name.setText(name);
            }
        }

        @Override
        public void onCancelled(DatabaseError databaseError) {

        }
    };
    private Conversation mConversation;
    private String uID, name, key;
    private Activity mActivity;

    public ConversationHolder(View itemView) {
        super(itemView);
        mBinding = DataBindingUtil.bind(itemView);
        imageSet = false;
    }

    public void bind(Conversation conversation, String uId, String key, final MessagesFragment.OnConversationClickedListener listener, final Activity activity) {
        uID = uId;
        if (uId == null) {
            Log.e(TAG, "bind: UID IS NULL" );
        }
        this.key = key;
        imageSet = false;
        mActivity = activity;
        mConversation = conversation;
        FirebaseDatabase.getInstance().getReference("users")
                .child(conversation.getOtherUser(uId))
                .child("info").addValueEventListener(mNameListener);
        mBinding.date.setText(Util.getMessageDateFormatted(conversation.getDate()));
        mBinding.lastMessage.setText(getProperLastMessageText(conversation, uId));
        mBinding.lastMessage.setTypeface(conversation.isLastMessageRead() || conversation.getLastMessageSender().equals(uID)? Typeface.DEFAULT : Typeface.DEFAULT_BOLD);
        mBinding.mainView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {listener.onClicked(mBinding.profileImage, mConversation, ConversationHolder.this.key, name);}
        });
        setProfileImage(ProfileImageDatabase.getProfileImage(conversation.getOtherUser(uId)));
    }

    private void setProfileImage(File image) {
        if (image == null) {
            mBinding.profileImage
                    .setImageDrawable(Util.getDrawableCompat(mBinding.mainView.getContext(), R.drawable.temp_profile));
            Util.getProfilePic(mConversation.getOtherUser(uID), mProfileSuccessListener, null);
        } else {
            Log.d(TAG, "bind: have the image!");
            if (!imageSet && mActivity != null && !mActivity.isDestroyed()) {
                Glide.with(mBinding.mainView.getContext())
                        .load(image)
                        .crossFade(350)
                        .into(mBinding.profileImage);
                imageSet = true;
            } else {
                Log.w(TAG, "setProfileImage: image was already set! listener overflow");
            }
        }
    }

    private String getProperLastMessageText(Conversation conversation, String uId) {
        Log.d(TAG, "getProperLastMessageText: lastMessageSender = " + conversation.getLastMessageSender());
        Log.d(TAG, "getProperLastMessageText: date = " + conversation.getDate());
        Log.d(TAG, "getProperLastMessageText: messages = " + conversation.getMessages());
        Context context = mBinding.mainView.getContext();

        return conversation.getLastMessageSender().equals(uId)
                ? (context.getString(R.string.you) + ": " + conversation.getLastMessage())
                : (conversation.getLastMessage());
    }

    public void cleanup() {
        name = null;
        mConversation = null;
        FirebaseDatabase.getInstance().getReference("users")
                .child(uID).child("info").removeEventListener(mNameListener);
        uID = null;
        mActivity = null;
    }
}
