package com.myspot.android.myspot.databases;

import android.support.v4.util.ArrayMap;
import android.util.Log;

import com.google.android.gms.maps.model.Marker;
import com.myspot.android.myspot.models.Listing;
import com.myspot.android.myspot.models.Spot;
import com.myspot.android.myspot.models.SpotMarker;
import com.myspot.android.myspot.utils.Util;

import java.util.Map;

public class MapDatabase {

    public static class MapListings {
        private static final String TAG = MapListings.class.getSimpleName();

        private static final Map<String, SpotMarker> mMarkersByListingId = new ArrayMap<>();
        private static final Map<String, Listing> mListings = new ArrayMap<>();
        private static final Map<String, SpotMarker> mMakersByMarkerId = new ArrayMap<>();

        public static synchronized void putMarker(SpotMarker marker) {
            Log.d(TAG, "putMarker: Marker id = " + marker.getMarker().getId());
            Log.d(TAG, "putMarker: Marker Listing id = " + marker.getListingId());
            mMarkersByListingId.put(marker.getListingId(), marker);
            mMakersByMarkerId.put(marker.getMarker().getId(), marker);
        }

        public static synchronized void putListing(Listing listing, String key) {
            Log.d(TAG, "putListing: Marker id = " + key);
            SpotMarker entry = mMarkersByListingId.get(key);
            if (entry != null) {
                mListings.put(entry.getMarker().getId(), listing);
            } else {
                Log.w(TAG, "putListing: no corresponding marker was found for listing " + key);
            }
            mListings.put(key, listing);
        }

        public static Marker removeMarkerByListingKey(String listingKey) {
            Marker ret = null;
            SpotMarker markerEntry = mMarkersByListingId.get(listingKey);
            if (markerEntry != null) {
                ret = mMarkersByListingId.remove(listingKey).getMarker();
                mMakersByMarkerId.remove(ret.getId());
            } else {
                Log.w(TAG, "getMarkerFromListing: no entry found for listing " + listingKey);
            }

            return ret;
        }

        public static Listing getListingFromMarker(String markerId) {
            Listing ret = null;
            SpotMarker entry = mMakersByMarkerId.get(markerId);
            if (entry != null) {
                ret = mListings.get(entry.getListingId());
            } else {
                Log.w(TAG, "getMarkerFromListing: no entry found listing with markerId " + markerId);
            }

            return ret;
        }

        public static SpotMarker getMarkerByMarkerId(String markerId) {
            return mMakersByMarkerId.get(markerId);
        }
    }

}
