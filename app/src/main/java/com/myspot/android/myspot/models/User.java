package com.myspot.android.myspot.models;

import java.util.HashMap;

public class User {

    /**
     * info : {"firstName":"Test","lastName":"Test"}
     * settings : {"mapType":"standard"}
     */

    private Info info;
    private Settings settings;
    private Bookings bookings;
    private Conversations conversations;
    private String merchantStatus;

    public User() {
        info = new Info();
        settings = new Settings();
        bookings = new Bookings();
        conversations = new Conversations();
        merchantStatus = "pending";
    }

    public String getMerchantStatus() {
        return merchantStatus;
    }

    public User setMerchantStatus(String merchantStatus) {
        this.merchantStatus = merchantStatus;
        return this;
    }

    public Info getInfo() {
        return info;
    }

    public User setInfo(Info info) {
        this.info = info;
        return this;
    }

    public Settings getSettings() {
        return settings;
    }

    public User setSettings(Settings settings) {
        this.settings = settings;
        return this;
    }

    public Bookings getBookings() {
        return bookings;
    }

    public void setBookings(Bookings bookings) {
        this.bookings = bookings;
    }

    public Conversations getConversations() {
        return conversations;
    }

    public void setConversations(Conversations conversations) {
        this.conversations = conversations;
    }

    public static class Info {
        /**
         * firstName : Test
         * lastName : Test
         */

        private String firstName;
        private String lastName;

        public String getFirstName() {
            return firstName;
        }

        public Info setFirstName(String firstName) {
            this.firstName = firstName;
            return this;
        }

        public String getLastName() {
            return lastName;
        }

        public Info setLastName(String lastName) {
            this.lastName = lastName;
            return this;
        }

        @Override
        public String toString() {
            return "info={firstName: "
                    + (firstName == null ? "NULL" : firstName)
                    + ", lastName: "
                    + (lastName == null ? "NULL" : lastName)
                    + "}";
        }
    }

    public static class Settings {
        /**
         * mapType : standard
         */

        private String mapType;

        public String getMapType() {
            return mapType;
        }

        public Settings setMapType(String mapType) {
            this.mapType = mapType;
            return this;
        }

        @Override
        public String toString() {
            return "settings={mapType: " + mapType + "}";
        }
    }

    public static class Bookings {
        HashMap<String, String> list;

        public Bookings() {
            list = new HashMap<>();
        }

        public HashMap<String, String> getList(){return list;}

        @Override
        public String toString() {
            String ret = "bookings={";

            for (String str : list.keySet()) {
                ret += str + ": " + list.get(str) + ", ";
            }

            return ret + "}";
        }
    }

    public static class Conversations {
        HashMap<String, String> list;

        public Conversations() {
            list = new HashMap<>();
        }

        public HashMap<String, String> getList() {
            return list;
        }

        @Override
        public String toString() {
            String ret = "conversations={";
            for (String str : list.keySet()) {
                ret += str + ": " + list.get(str) + ", ";
            }
            return ret + "}";
        }
    }

    @Override
    public String toString() {
        return "User {" + info.toString() + "\n" + bookings.toString() + "\n " + conversations.toString() + "\n " + settings.toString() + "}";
    }
}
