package com.myspot.android.myspot.utils;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.FirebaseDatabase;
import com.myspot.android.myspot.models.Conversation;
import com.myspot.android.myspot.models.Message;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public class MessageUtil {
    private static final String TAG = MessageUtil.class.getSimpleName();

    public static final DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss", Locale.getDefault());

    public static void sendMessage(String message, String lastMessageSender, final Conversation conversation, final String convId,
                                   final OnSuccessListener<Void> successListener,
                                   OnFailureListener failureListener) {
        final Message newMessage = new Message();
        newMessage.setDate(dateFormat.format(new Date()));
        newMessage.setMessage(message);
        newMessage.setSenderId(FirebaseAuth.getInstance().getCurrentUser().getUid());
        newMessage.setSenderName(lastMessageSender);

        String newKey = FirebaseDatabase.getInstance().getReference("messages")
                .child(conversation.getMessages()).push().getKey();

        FirebaseDatabase.getInstance().getReference("messages").child(conversation.getMessages())
                .child(newKey)
                .setValue(newMessage)
                .addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {
                        successListener.onSuccess(aVoid);

                        Conversation newConversation = new Conversation(conversation);
                        newConversation.setDate(newMessage.getDate());
                        newConversation.setLastMessage(newMessage.getMessage());
                        newConversation.setLastMessageSender(FirebaseAuth.getInstance().getCurrentUser().getUid());
                        newConversation.setLastMessageRead(false);

                        FirebaseDatabase.getInstance().getReference("conversations").child(convId)
                                .setValue(newConversation);
                    }
                })
                .addOnFailureListener(failureListener);
    }

}
