package com.myspot.android.myspot.models;

import java.util.List;

public class Spot {

    /**
     * address : {"city":"San Luis Obispo","country":"US","postalCode":"93405","state":"CA","street":"481 Ramona Dr"}
     * description : Dope spot
     * image : 506594527630.jpg
     * location : {"latitude":35.29195224810528,"longitude":-120.6769231096347}
     * name : Driveway
     * owner : f2sTDdqD71QBaXn6QcBqFF2e2qR2
     * supportedVehicles : ["Car","Truck","Motorcycle"]
     */

    private Address address;
    private String description;
    private String image;
    private Location location;
    private String name;
    private String owner;
    private List<String> supportedVehicles;

    public Address getAddress() {
        return address;
    }

    public void setAddress(Address address) {
        this.address = address;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public Location getLocation() {
        return location;
    }

    public void setLocation(Location location) {
        this.location = location;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getOwner() {
        return owner;
    }

    public void setOwner(String owner) {
        this.owner = owner;
    }

    public List<String> getSupportedVehicles() {
        return supportedVehicles;
    }

    public void setSupportedVehicles(List<String> supportedVehicles) {
        this.supportedVehicles = supportedVehicles;
    }

    public static class Address {
        /**
         * city : San Luis Obispo
         * country : US
         * postalCode : 93405
         * state : CA
         * street : 481 Ramona Dr
         */

        private String city;
        private String country;
        private String postalCode;
        private String state;
        private String street;

        public String getCity() {
            return city;
        }

        public void setCity(String city) {
            this.city = city;
        }

        public String getCountry() {
            return country;
        }

        public void setCountry(String country) {
            this.country = country;
        }

        public String getPostalCode() {
            return postalCode;
        }

        public void setPostalCode(String postalCode) {
            this.postalCode = postalCode;
        }

        public String getState() {
            return state;
        }

        public void setState(String state) {
            this.state = state;
        }

        public String getStreet() {
            return street;
        }

        public void setStreet(String street) {
            this.street = street;
        }

        @Override
        public String toString() {
            return street + " " + city;
        }
    }

    public static class Location {
        /**
         * latitude : 35.29195224810528
         * longitude : -120.6769231096347
         */

        private double latitude;
        private double longitude;

        public double getLatitude() {
            return latitude;
        }

        public void setLatitude(double latitude) {
            this.latitude = latitude;
        }

        public double getLongitude() {
            return longitude;
        }

        public void setLongitude(double longitude) {
            this.longitude = longitude;
        }
    }
}
