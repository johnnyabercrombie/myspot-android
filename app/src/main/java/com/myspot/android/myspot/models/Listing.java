package com.myspot.android.myspot.models;

import android.content.Context;
import android.util.Log;

import com.esafirm.imagepicker.model.Image;
import com.google.android.gms.location.places.Place;
import com.myspot.android.myspot.MainActivity;
import com.myspot.android.myspot.R;
import com.myspot.android.myspot.utils.Util;

import java.text.NumberFormat;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

import static android.content.ContentValues.TAG;

public class Listing {
    public static final String HOURLY = "Hourly", BLOCK = "Block", FULL = "Full";

    private long cost;
    private String ends;
    private String rate;
    private String spot;
    private String starts;
    private String owner;
    private String description;
    private Location location;
    private List<Availability> availability;
    private List<String> images;

    public Listing() {}

    public Listing(long cost, String ends, String rate, String spot, String starts, String owner, List<Availability> availability, Place mPlace, String description, String address, List<Image> imageList) {
        this.cost = cost;
        this.ends = ends;
        this.rate = rate;
        this.spot = spot;
        this.starts = starts;
        this.owner = owner;
        this.location = new Location(mPlace, address);
        this.description = description;
        this.availability = availability;
        this.images = new ArrayList<>();
        for (int i = 0; i < imageList.size(); i++) {
            images.add("image" + i);
        }
    }

    public List<String> getImages() {
        return images;
    }

    public void setImages(List<String> images) {
        this.images = images;
    }

    public Location getLocation() {
        return location;
    }

    public void setLocation(Location location) {
        this.location = location;
    }

    public List<Availability> getAvailability() {
        return availability;
    }

    public void setAvailability(List<Availability> availability) {
        this.availability = availability;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public long getCost() {
        return cost;
    }

    public void setCost(long cost) {
        this.cost = cost;
    }

    public String getEnds() {
        return ends;
    }

    public void setEnds(String ends) {
        this.ends = ends;
    }

    public String getRate() {
        return rate;
    }

    public void setRate(String rate) {
        this.rate = rate;
    }

    public String getSpot() {
        return spot;
    }

    public void setSpot(String spot) {
        this.spot = spot;
    }

    public String getStarts() {
        return starts;
    }

    public void setStarts(String starts) {
        this.starts = starts;
    }

    public String getOwner() {
        return owner;
    }

    public void setOwner(String owner) {
        this.owner = owner;
    }

    public String getPriceText(Context context) {
        NumberFormat numberFormat = NumberFormat.getCurrencyInstance();
        return numberFormat.format(cost / 10) + (rate.equals("hourly")
                ? " " + context.getString(R.string.per_hour)
                : " " + context.getString(R.string.fixed_paren));
    }

    public String getAvailableFromText() {
        try {
            return Util.rangeDateFormat.format(Util.firebaseDateFormat.parse(starts))
                    + " @ "
                    + Util.rangeTimeFormat.format(Util.firebaseDateFormat.parse(starts));
        } catch (ParseException e) {
            Log.e(TAG, "getAvailableFromText: " + Log.getStackTraceString(e));
            return starts;
        }
    }

    public String getAvailableUntilText() {
        try {
            return Util.rangeDateFormat.format(Util.firebaseDateFormat.parse(ends))
                    + " @ "
                    + Util.rangeTimeFormat.format(Util.firebaseDateFormat.parse(ends));
        } catch (ParseException e) {
            Log.e(TAG, "getAvailableUntilText: " + Log.getStackTraceString(e));
            return ends;
        }
    }

    public String getDescriptionText(Context context) {
        return description == null ? context.getString(R.string.no_description) : description;
    }

    public static class Availability {
        private String startTime, endTime;

        public Availability() {}

        public Availability(String startTime, String endTime) {
            this.startTime = startTime;
            this.endTime = endTime;
        }

        public String getStartTime() {
            return startTime;
        }

        public void setStartTime(String startTime) {
            this.startTime = startTime;
        }

        public String getEndTime() {
            return endTime;
        }

        public void setEndTime(String endTime) {
            this.endTime = endTime;
        }
    }

    public static class Location {
        String address, name;
        Double latitude, longitude;

        public Location(){}

        public Location(Place mPlace, String address) {
            if (mPlace != null) {
                latitude = mPlace.getLatLng().latitude;
                longitude = mPlace.getLatLng().longitude;
                this.address = address == null
                        ? String.valueOf(mPlace.getAddress())
                        : address;
                name = String.valueOf(mPlace.getName());
            }
        }

        public Double getLatitude() {
            return latitude;
        }

        public void setLatitude(Double latitude) {
            this.latitude = latitude;
        }

        public Double getLongitude() {
            return longitude;
        }

        public void setLongitude(Double longitude) {
            this.longitude = longitude;
        }

        public String getAddress() {
            return address;
        }

        public void setAddress(String address) {
            this.address = address;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }
    }
}
