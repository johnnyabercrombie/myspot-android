package com.myspot.android.myspot.models;

import com.myspot.android.myspot.utils.Util;

import java.util.Date;
import java.util.List;

public class Booking {

    private long cost;
    private String ends;
    private String rate;
    private String spot;
    private String starts;
    private String owner;
    private String description;
    private Listing.Location location;
    private List<Listing.Availability> availability;
    private List<String> images;
    private String buyer;
    private String purchaseDate;

    public Booking(){}

    public Booking(Listing listing, String buyer) {
        this.cost = listing.getCost();
        this.ends = listing.getEnds();
        this.rate = listing.getRate();
        this.spot = listing.getSpot();
        this.starts = listing.getStarts();
        this.owner = listing.getOwner();
        this.description = listing.getDescription();
        this.location = listing.getLocation();
        this.availability = listing.getAvailability();
        this.images = listing.getImages();
        this.buyer = buyer;
        this.purchaseDate = Util.firebaseDateFormat.format(new Date());

    }

    public String getBuyer() {
        return buyer;
    }

    public void setBuyer(String buyer) {
        this.buyer = buyer;
    }

    public String getPurchaseDate() {
        return purchaseDate;
    }

    public void setPurchaseDate(String purchaseDate) {
        this.purchaseDate = purchaseDate;
    }

    public long getCost() {
        return cost;
    }

    public void setCost(long cost) {
        this.cost = cost;
    }

    public String getEnds() {
        return ends;
    }

    public void setEnds(String ends) {
        this.ends = ends;
    }

    public String getRate() {
        return rate;
    }

    public void setRate(String rate) {
        this.rate = rate;
    }

    public String getSpot() {
        return spot;
    }

    public void setSpot(String spot) {
        this.spot = spot;
    }

    public String getStarts() {
        return starts;
    }

    public void setStarts(String starts) {
        this.starts = starts;
    }

    public String getOwner() {
        return owner;
    }

    public void setOwner(String owner) {
        this.owner = owner;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Listing.Location getLocation() {
        return location;
    }

    public void setLocation(Listing.Location location) {
        this.location = location;
    }

    public List<Listing.Availability> getAvailability() {
        return availability;
    }

    public void setAvailability(List<Listing.Availability> availability) {
        this.availability = availability;
    }

    public List<String> getImages() {
        return images;
    }

    public void setImages(List<String> images) {
        this.images = images;
    }
}
