package com.myspot.android.myspot.adapters;

import android.support.annotation.LayoutRes;
import android.support.v7.widget.RecyclerView;

import com.firebase.ui.database.FirebaseIndexRecyclerAdapter;
import com.google.firebase.database.Query;

public class ConversationFirebaseAdapter extends FirebaseIndexRecyclerAdapter {
    private String uID;

    public ConversationFirebaseAdapter(Class modelClass, @LayoutRes int modelLayout, Class viewHolderClass, Query keyRef, Query dataRef, String uID) {
        super(modelClass, modelLayout, viewHolderClass, keyRef, dataRef);
        this.uID = uID;
    }

    @Override
    protected void populateViewHolder(RecyclerView.ViewHolder viewHolder, Object model, int position) {}

    public String getuID() {
        return uID;
    }
}
