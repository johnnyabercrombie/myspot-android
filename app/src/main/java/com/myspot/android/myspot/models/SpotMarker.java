package com.myspot.android.myspot.models;

import com.google.android.gms.maps.model.Marker;

public class SpotMarker {
    private Marker mMarker;
    private String mListingId;

    public SpotMarker(Marker marker, String listingKey) {
        this.mMarker = marker;
        this.mListingId = listingKey;
    }

    public Marker getMarker() {
        return mMarker;
    }

    public String getListingId() {
        return mListingId;
    }
}
