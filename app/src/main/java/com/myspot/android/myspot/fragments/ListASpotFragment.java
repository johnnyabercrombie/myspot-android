package com.myspot.android.myspot.fragments;

import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.DatePicker;
import android.widget.TimePicker;
import android.widget.Toast;

import com.afollestad.materialdialogs.MaterialDialog;
import com.esafirm.imagepicker.features.ImagePicker;
import com.esafirm.imagepicker.model.Image;
import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlacePicker;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.FirebaseDatabase;
import com.myspot.android.myspot.MainActivity;
import com.myspot.android.myspot.R;
import com.myspot.android.myspot.adapters.ImagesAdapter;
import com.myspot.android.myspot.databinding.FragmentListASpotBinding;
import com.myspot.android.myspot.models.Listing;
import com.myspot.android.myspot.utils.ListSpotUtil;
import com.myspot.android.myspot.utils.Util;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import github.hellocsl.layoutmanager.gallery.GalleryLayoutManager;

import static android.app.Activity.RESULT_OK;
import static com.myspot.android.myspot.utils.Constants.REQUEST_IMAGE_PICK;
import static com.myspot.android.myspot.utils.Constants.REQUEST_PLACE_PICK;

public class ListASpotFragment extends Fragment {

    private static final String TAG = ListASpotFragment.class.getSimpleName();
    private FragmentListASpotBinding mBinding;

    public ImagesAdapter mAdapter;

    private Calendar mDateFromSelected, mDateUntilSelected;

    private Date dateFrom, dateUntil;
    private Place mPlace;

    public ListASpotFragment() {
        // Required empty public constructor
    }

    public static ListASpotFragment newInstance() {
        ListASpotFragment fragment = new ListASpotFragment();
        Bundle args = new Bundle();

        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        dateFrom = new Date();
        Calendar cal = Calendar.getInstance();
        cal.setTime(dateFrom);
        cal.add(Calendar.DAY_OF_MONTH, 1);
        dateUntil = cal.getTime();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        mBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_list_a_spot, container, false);

        if (mAdapter == null) {
            mAdapter = new ImagesAdapter(this);
        } else {
            mAdapter.setContext(this);
        }

        mDateUntilSelected = Calendar.getInstance();
        mDateFromSelected = Calendar.getInstance();

        dateUntil = ((MainActivity) getActivity()).getSavedData().getListingDateUntil();
        dateFrom = ((MainActivity) getActivity()).getSavedData().getListingDateFrom();
        if (dateFrom == null) {
            dateFrom = new Date();
        }
        if (dateUntil == null) {
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(dateFrom);
            calendar.add(Calendar.DAY_OF_MONTH, 1);
            dateUntil = calendar.getTime();
        }

        mBinding.listingStartDate.setText(Util.rangeDateFormat.format(dateFrom));
        mBinding.startTime.setText(Util.rangeTimeFormat.format(dateFrom));
        mBinding.untilDate.setText(Util.rangeDateFormat.format(dateUntil));
        mBinding.untilTime.setText(Util.rangeTimeFormat.format(dateUntil));

        GalleryLayoutManager layoutManager = new GalleryLayoutManager(GalleryLayoutManager.HORIZONTAL);
        layoutManager.attach(mBinding.listingImageList);
        layoutManager.setItemTransformer(new GalleryLayoutManager.ItemTransformer() {
            @Override
            public void transformItem(GalleryLayoutManager layoutManager, View item, float fraction) {
                item.setPivotX(item.getWidth() / 2.f);
                item.setPivotY(item.getHeight()/2.0f);
                float scale = 1 - 0.3f * Math.abs(fraction);
                item.setScaleX(scale);
                item.setScaleY(scale);
            }
        });
        mBinding.listingImageList.setAdapter(mAdapter);

        mBinding.listingStartDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {onDateFromClicked();
            }
        });
        mBinding.startTime.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onTimeFromClicked();
            }
        });
        mBinding.untilDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onDateUntilClicked();
            }
        });
        mBinding.untilTime.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {onTimeUntilClicked();
            }
        });
        mBinding.createListing.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                checkInputs();
            }
        });
        mBinding.selectLocation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                selectLocation();
            }
        });

        return mBinding.mainView;
    }

    private void selectLocation() {
        PlacePicker.IntentBuilder builder = new PlacePicker.IntentBuilder();

        try {
            startActivityForResult(builder.build(getActivity()), REQUEST_PLACE_PICK);
        } catch (Exception e) {
            MaterialDialog dialog = new MaterialDialog.Builder(getContext())
                    .title(R.string.err_play_services_title)
                    .content(R.string.err_play_services_content)
                    .positiveText(android.R.string.ok)
                    .cancelable(false)
                    .build();
            dialog.show();
        }
    }

    private void checkInputs() {
        boolean error = false;
        String locationText = mBinding.locationText.getText().toString();
        String addressOneText = mBinding.spotAddressOne.getText().toString();
        String priceText = mBinding.price.getText().toString();

        clearErrors();

        if (mDateFromSelected == null) {
            mBinding.startDateLayout.setError(getString(R.string.err_select_date));
            error = true;
        }

        if (mDateUntilSelected == null) {
            mBinding.untilDateLayout.setError(getString(R.string.err_select_date));
            error = true;
        }

        if (TextUtils.isEmpty(locationText)) {
            mBinding.locationTextLayout.setError(getString(R.string.err_select_location));
            error = true;
        }

        if (TextUtils.isEmpty(addressOneText)) {
            mBinding.addressInputLayout.setError(getString(R.string.err_address_input));
        }

        if (TextUtils.isEmpty(priceText)) {
            mBinding.priceInputLayout.setError(getString(R.string.err_price_input));
            error = true;
        }

        if (mAdapter.getImageList().size() == 0) {
            Toast.makeText(getContext(), R.string.err_image_input, Toast.LENGTH_SHORT).show();
            //Snackbar.make(mBinding.getRoot(), R.string.err_image_input, Snackbar.LENGTH_SHORT).show();
            error = true;
        }

        if (!error) {
            addListing(addressOneText);
        }
    }

    private void addListing(String addressOneText) {
        String user = FirebaseAuth.getInstance().getCurrentUser().getUid();
        String startDate = Util.firebaseDateFormat.format(dateFrom);
        String endDate = Util.firebaseDateFormat.format(dateUntil);
        String description = mBinding.description.getText().toString();
        ArrayList<Listing.Availability> list = new ArrayList<>();
        list.add(new Listing.Availability(startDate, endDate));

        Listing listing = new Listing(getListingPrice(),
                endDate,
                getListingRate(),
                "UNUSED",
                startDate,
                user,
                list,
                mPlace,
                description,
                addressOneText,
                mAdapter.getImageList());

        ListSpotUtil.addListing(listing, user, mAdapter.getImageList());
        resetView(true);
    }

    private void resetView(boolean showNewListingMessage) {
        dateFrom = new Date();
        Calendar c = Calendar.getInstance();
        c.setTime(dateFrom);
        c.add(Calendar.DAY_OF_MONTH, 1);
        dateUntil = c.getTime();
        mDateFromSelected.setTime(dateFrom);
        mDateUntilSelected.setTime(dateUntil);

        mBinding.listingStartDate.setText(Util.rangeDateFormat.format(dateFrom));
        mBinding.startTime.setText(Util.rangeTimeFormat.format(dateFrom));
        mBinding.untilDate.setText(Util.rangeDateFormat.format(dateUntil));
        mBinding.untilTime.setText(Util.rangeTimeFormat.format(dateUntil));
        mBinding.spotAddressOne.setText("");
        mBinding.price.setText("");
        mBinding.description.setText("");

        if (showNewListingMessage) {
            Toast.makeText(getContext(), R.string.listing_created, Toast.LENGTH_SHORT).show();
        }
    }

    private void clearErrors() {
        mBinding.startDateLayout.setError(null);
        mBinding.untilDateLayout.setError(null);
        mBinding.locationTextLayout.setError(null);
        mBinding.priceInputLayout.setError(null);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            case REQUEST_IMAGE_PICK:
                if (resultCode == RESULT_OK) {
                    List<Image> images = ImagePicker.getImages(data);
                    mAdapter.addImages(images);
                }
                break;
            case REQUEST_PLACE_PICK:
                if (resultCode == RESULT_OK) {
                    mPlace = PlacePicker.getPlace(getContext(), data);
                    mBinding.spotAddressOne.setText(mPlace.getAddress());
                    mBinding.locationText.setText(mPlace.getName());
                    mBinding.locationTextLayout.setError(null);
                }
                break;
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onDetach() {
        mAdapter.cleanup();
        super.onDetach();
    }

    @Override
    public void onDestroyView() {
        MainActivity activity = (MainActivity) getActivity();
        if (activity != null) {
            if (dateUntil != null) {
                activity.getSavedData().setListingDateUntil(dateUntil);
            }
            if (dateFrom != null) {
                activity.getSavedData().setListingDateFrom(dateFrom);
            }
        }
        super.onDestroyView();
    }

    private void onTimeFromClicked() {
        mDateFromSelected = Calendar.getInstance();
        mDateFromSelected.setTime(dateFrom);
        new TimePickerDialog(getContext(), new TimePickerDialog.OnTimeSetListener() {
            @Override
            public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                mDateFromSelected.set(Calendar.HOUR_OF_DAY, hourOfDay);
                mDateFromSelected.set(Calendar.MINUTE, minute);
                Calendar today = Calendar.getInstance();
                today.setTime(new Date());
                if (mDateFromSelected.after(today)) {
                    dateFrom = mDateFromSelected.getTime();
                    mBinding.startTime.setText(Util.rangeTimeFormat.format(dateFrom));
                    if (dateUntil.before(dateFrom)) {
                        Log.d(TAG, "onTimeSet: dateUntil before dateFrom!");
                        mDateUntilSelected.setTime(dateFrom);
                        mDateUntilSelected.add(Calendar.DAY_OF_MONTH, 1);
                        dateUntil = mDateUntilSelected.getTime();
                        mBinding.untilDate.setText(Util.rangeDateFormat.format(dateUntil));
                    }
                } else {
                    Toast.makeText(getContext(), R.string.err_time_before_today, Toast.LENGTH_LONG).show();
                    //Snackbar.make(mBinding.mapLayout, R.string.err_time_before_today, Snackbar.LENGTH_SHORT).show();
                }
            }
        }, mDateFromSelected.get(Calendar.HOUR_OF_DAY), mDateFromSelected.get(Calendar.MINUTE), false)
                .show();
    }

    private void onDateFromClicked() {
        mDateFromSelected = Calendar.getInstance();
        mDateFromSelected.setTime(dateFrom);
        new DatePickerDialog(getContext(), new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                mDateFromSelected.set(Calendar.YEAR, year);
                mDateFromSelected.set(Calendar.MONTH, month);
                mDateFromSelected.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                Calendar today = Calendar.getInstance();
                today.setTime(new Date());
                if (mDateFromSelected.after(today)) {
                    dateFrom = mDateFromSelected.getTime();
                    mBinding.listingStartDate.setText(Util.rangeDateFormat.format(dateFrom));
                    if (dateUntil.before(dateFrom)) {
                        mDateUntilSelected.setTime(dateFrom);
                        mDateUntilSelected.add(Calendar.DATE, 1);
                        dateUntil = mDateFromSelected.getTime();
                        mBinding.untilDate.setText(Util.rangeDateFormat.format(dateUntil));
                    }
                } else {
                    Toast.makeText(getContext(), R.string.err_time_before_today, Toast.LENGTH_LONG).show();
                    //Snackbar.make(mBinding.mapLayout, R.string.err_time_before_today, Snackbar.LENGTH_SHORT).show();
                }
            }
        }, mDateFromSelected.get(Calendar.YEAR), mDateFromSelected.get(Calendar.MONTH), mDateFromSelected.get(Calendar.DAY_OF_MONTH))
                .show();
    }

    private void onTimeUntilClicked() {
        mDateUntilSelected = Calendar.getInstance();
        mDateUntilSelected.setTime(dateFrom);
        new TimePickerDialog(getContext(), new TimePickerDialog.OnTimeSetListener() {
            @Override
            public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                mDateUntilSelected.set(Calendar.HOUR_OF_DAY, hourOfDay);
                mDateUntilSelected.set(Calendar.MINUTE, minute);
                Calendar today = Calendar.getInstance();
                today.setTime(new Date());
                if (mDateUntilSelected.after(today)) {
                    Calendar fromC = Calendar.getInstance();
                    fromC.setTime(dateFrom);
                    if (mDateUntilSelected.before(fromC)) {
                        Snackbar.make(mBinding.getRoot(), R.string.err_time_before_from, Snackbar.LENGTH_SHORT).show();
                    } else {
                        dateUntil = mDateUntilSelected.getTime();
                        mBinding.untilTime.setText(Util.rangeTimeFormat.format(dateUntil));
                    }
                } else {
                    Snackbar.make(mBinding.getRoot(), R.string.err_time_before_today, Snackbar.LENGTH_SHORT).show();
                    //Snackbar.make(mBinding.getRoot(), R.string.err_time_before_today, Snackbar.LENGTH_SHORT).show();
                }
            }
        }, mDateUntilSelected.get(Calendar.HOUR_OF_DAY), mDateUntilSelected.get(Calendar.MINUTE), false) // TODO: 5/18/2017 Set 24hr mode based on location
                .show();
    }

    private void onDateUntilClicked() {
        mDateUntilSelected = Calendar.getInstance();
        mDateUntilSelected.setTime(dateUntil);
        DatePickerDialog datePickerDialog = new DatePickerDialog(getContext(), new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                mDateUntilSelected.set(Calendar.YEAR, year);
                mDateUntilSelected.set(Calendar.MONTH, month);
                mDateUntilSelected.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                Calendar today = Calendar.getInstance();
                today.setTime(new Date());
                if (mDateUntilSelected.after(today)) {
                    Calendar fromC = Calendar.getInstance();
                    fromC.setTime(dateFrom);
                    if (mDateUntilSelected.before(fromC)) {
                        Toast.makeText(getContext(), R.string.err_time_before_from, Toast.LENGTH_LONG).show();
                        //Snackbar.make(mBinding.mapLayout, R.string.err_time_before_from, Snackbar.LENGTH_SHORT).show();
                    } else {
                        dateUntil = mDateUntilSelected.getTime();
                        mBinding.untilDate.setText(Util.rangeDateFormat.format(dateUntil));
                    }
                } else {
                    Toast.makeText(getContext(), R.string.err_date_before_today, Toast.LENGTH_LONG).show();
                    //Snackbar.make(mBinding.mapLayout, R.string.err_date_before_today, Snackbar.LENGTH_SHORT).show();
                }
            }
        }, mDateUntilSelected.get(Calendar.YEAR), mDateUntilSelected.get(Calendar.MONTH), mDateUntilSelected.get(Calendar.DAY_OF_MONTH));
        datePickerDialog.show();
    }

    public Date getListingDateFrom() {
        return dateFrom;
    }

    public Date getListingDateUntil() {
        return dateUntil;
    }

    public String getListingSpotAddressOne() {
        return mBinding.spotAddressOne.getText().toString();
    }

    public int getListingPrice() {
        BigDecimal b = null;
        try {
            b = new java.math.BigDecimal(mBinding.price.getText().toString());
            b = b.multiply(new BigDecimal(100));
        } catch (Exception e) {
            Log.e(TAG, "getListingPrice: " + Log.getStackTraceString(e));
        }


        return b == null ? -1 : b.intValue();
    }

    public String getListingRate() {
        return (String) mBinding.spinner.getSelectedItem();
    }

    public void setdateFrom(Date dateFrom) {
        this.dateFrom = dateFrom;
    }

    public void setDateUntil(Date dateUntil) {
        this.dateUntil = dateUntil;
    }
}
