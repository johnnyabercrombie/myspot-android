package com.myspot.android.myspot.fragments;


import android.content.Context;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.method.KeyListener;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.bumptech.glide.Glide;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FileDownloadTask;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.myspot.android.myspot.MainActivity;
import com.myspot.android.myspot.R;
import com.myspot.android.myspot.databases.ProfileImageDatabase;
import com.myspot.android.myspot.databinding.FragmentProfileBinding;
import com.myspot.android.myspot.models.User;
import com.myspot.android.myspot.utils.SignInUtils;
import com.myspot.android.myspot.utils.Util;

import java.io.File;
import java.io.IOException;
import java.sql.Struct;
import java.util.HashMap;

import static com.myspot.android.myspot.utils.Constants.USER_PREFS;
import static com.myspot.android.myspot.utils.Constants.USER_PREFS_PROFILE_URL;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link ProfileFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class ProfileFragment extends Fragment {
    private static final String TAG = ProfileFragment.class.getSimpleName();
    public static final String ARG_EDIT_MODE = "ARG_EDIT_MODE";

    private FragmentProfileBinding mBinding;
    private int mActiveListingCount, mActiveBookingCount, mActiveRentalCount;
    private boolean inEditMode;

    private ValueEventListener mActiveListingListener = new ValueEventListener() {
        @Override
        public void onDataChange(DataSnapshot dataSnapshot) {
            if (mBinding != null) {
                if (dataSnapshot.getValue() != null) {
                    HashMap<String, String> values = (HashMap<String, String>) dataSnapshot.getValue();
                    mActiveListingCount = values.size();
                } else {
                    mActiveListingCount = 0;
                }
                setActiveListingsTV();
            } else {
                Log.w(TAG, "onDataChange: binding was null!");
            }

        }

        @Override
        public void onCancelled(DatabaseError databaseError) {
            Log.e(TAG, "onCancelled: " + Log.getStackTraceString(databaseError.toException()));
        }
    };
    private ValueEventListener mActiveBookingListener = new ValueEventListener() {
        @Override
        public void onDataChange(DataSnapshot dataSnapshot) {
            if (mBinding != null) {
                if (dataSnapshot.getValue() != null) {
                    HashMap<String, String> values = (HashMap<String, String>) dataSnapshot.getValue();
                    mActiveBookingCount = values.size();
                } else {
                    mActiveBookingCount = 0;
                }
                setActiveBookingsTV();
            } else {
                Log.w(TAG, "onDataChange: binding was null!");
            }
        }

        @Override
        public void onCancelled(DatabaseError databaseError) {
            Log.e(TAG, "onCancelled: " + Log.getStackTraceString(databaseError.toException()) );
        }
    };
    private ValueEventListener mActiveRentalListener = new ValueEventListener() {
        @Override
        public void onDataChange(DataSnapshot dataSnapshot) {
            if (mBinding != null) {
                if (dataSnapshot.getValue() != null) {
                    HashMap<String, String> values = (HashMap<String, String>) dataSnapshot.getValue();
                    mActiveRentalCount = values.size();
                } else {
                    mActiveRentalCount = 0;
                }
                setActiveRentalsTV();
            } else {
                Log.w(TAG, "onDataChange: binding was null!");
            }
        }

        @Override
        public void onCancelled(DatabaseError databaseError) {
            Log.e(TAG, "onCancelled: " + Log.getStackTraceString(databaseError.toException()) );
        }
    };
    private ValueEventListener mInfoListener = new ValueEventListener() {
        @Override
        public void onDataChange(DataSnapshot dataSnapshot) {
            if (mBinding != null) {
                if (dataSnapshot.getValue() != null) {
                    HashMap<String, String> values = (HashMap<String, String>) dataSnapshot.getValue();
                    String firstName = values.get("firstName");
                    String lastName = values.get("lastName");
                    mBinding.name.setText(firstName + " " + lastName);
                } else {
                    Log.e(TAG, "onDataChange: null value for User Info! " + dataSnapshot.getKey() );
                }
            } else {
                Log.w(TAG, "onDataChange: binding was null!");
            }
        }

        @Override
        public void onCancelled(DatabaseError databaseError) {
            Log.e(TAG, "onCancelled: " + Log.getStackTraceString(databaseError.toException()) );
        }
    };
    private Util.OnProfileSuccessListener mProfileSuccessListener = new Util.OnProfileSuccessListener() {
        @Override
        public void onSuccess(FileDownloadTask.TaskSnapshot taskSnapshot) {
            if (getuID() != null) {
                Log.d(TAG, "setupProfile: adding image");
                ProfileImageDatabase.addProfileImage(getuID(), getLocalFile());
            }
            if (mBinding != null && getContext() != null && getLocalFile() != null) {
                Glide.with(getContext())
                        .fromFile()
                        .load(getLocalFile())
                        .centerCrop()
                        .placeholder(R.drawable.temp_profile)
                        .crossFade()
                        .into(mBinding.profileImage);
            } else {
                Log.w(TAG, "onSuccess: binding or localFile was null!");
            }
            cleanUp();
        }
    };
    private OnFailureListener mProfileFailureListener = new OnFailureListener() {
        @Override
        public void onFailure(@NonNull Exception e) {
            Log.d(TAG, "onFailure: " + Log.getStackTraceString(e));
            if (getActivity() != null) {
                String profilePhoto = getActivity().getSharedPreferences(USER_PREFS, Context.MODE_PRIVATE)
                        .getString(USER_PREFS_PROFILE_URL, null);
                if (profilePhoto != null) {
                    Glide.with(getContext())
                            .load(profilePhoto)
                            .centerCrop()
                            .placeholder(R.drawable.temp_profile)
                            .crossFade()
                            .into(mBinding.profileImage);
                } else {
                    Log.e(TAG, "setupProfilePic:  profile URL was null!");
                }
            } else {
                Log.w(TAG, "onFailure: activity was null!");
            }
        }
    };

    public ProfileFragment() {}

    private void setActiveRentalsTV() {
        String str;
        if (mActiveRentalCount == 0) {
            str = getString(R.string.currently_renting) + " 0 " + getString(R.string.spots);
        } else {
            str = getString(R.string.currently_renting) + " "
                    + Integer.toString(mActiveRentalCount) + " " + getString(R.string.spots);
        }
        mBinding.currentRentals.setText(str);
    }

    private void setActiveListingsTV() {
        if (mBinding != null) {
            if (mActiveListingCount == 0) {
                mBinding.activeListingsCount.setText(getString(R.string.no_active_listings));
            } else {
                String countString = Integer.toString(mActiveListingCount) + " " + getString(R.string.active_listings);
                mBinding.activeListingsCount.setText(countString);
            }
        }
    }

    private void setActiveBookingsTV() {
        if (mBinding != null) {
            if (mActiveBookingCount == 0) {
                mBinding.bookedCount.setText(R.string.no_current_bookings);
            } else {
                String countString = Integer.toString(mActiveBookingCount) + " " + getString(R.string.current_bookings);
                mBinding.bookedCount.setText(countString);
            }
        }
    }

    public static ProfileFragment newInstance() {
        Log.d(TAG, "newInstance: ");
        ProfileFragment fragment = new ProfileFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        fragment.setActiveListingCount(0);
        fragment.setActiveBookingCount(0);
        fragment.setActiveRentalCount(0);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (getArguments() != null) {
            inEditMode = getArguments().getBoolean(ARG_EDIT_MODE, false);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_profile, container, false);

        FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
        if (user != null) {
            mBinding.email.setText(user.getEmail());
            setupProfilePic();
            setupEventListeners(user.getUid());
            //setupEditMode();
            mBinding.signOut.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {displaySignOutWarning();}
            });
            mBinding.floatingActionButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {toggleFAB();
                }
            });

        } else {
            Log.e(TAG, "setupProfilePic: user was null!");
        }

        return mBinding.profileMain;
    }

    private void setupEditMode() {
        if (mBinding != null) {
            if (inEditMode) {
//                if (mBinding.name.getTag() != null && mBinding.name.getTag().getClass() == KeyListener.class) {
//                    mBinding.name.setKeyListener((KeyListener) mBinding.name.getTag());
//                }
                mBinding.name.setFocusable(true);
                if (getContext() != null) {
                    mBinding.profileScrim
                            .setBackground(Util.getDrawableCompat(getContext(), R.drawable.scrim_full));
                    mBinding.floatingActionButton
                            .setBackgroundColor(Util.getColorCompat(getContext(), R.color.done_color));
                    mBinding.floatingActionButton.setImageResource(R.drawable.ic_done_white_24dp);
                }
            } else {
                if (getContext() != null) {
                    mBinding.profileScrim
                            .setBackground(Util.getDrawableCompat(getContext(), R.drawable.scrim_default));
                    mBinding.floatingActionButton
                            .setBackgroundColor(Util.getColorCompat(getContext(), R.color.colorAccent));
                    mBinding.floatingActionButton.setImageResource(R.drawable.ic_edit_white_24dp);
                }
                //mBinding.name.setTag(mBinding.name.getKeyListener());
                //mBinding.name.setKeyListener(null);
                mBinding.name.setFocusable(false);
            }
        }
    }

    private void toggleFAB() {
        if (inEditMode) {
            String[] values = Util.getNameStrings(mBinding.name.getText().toString());
            String uid = FirebaseAuth.getInstance().getCurrentUser().getUid();
            FirebaseDatabase.getInstance().getReference("users").child(uid).child("info")
                    .setValue(new User.Info()
                            .setFirstName(values[0])
                            .setLastName(values[1]));
        }
        inEditMode = !inEditMode;
        setupEditMode();
        Log.d(TAG, "toggleFAB: inEditMode = " + inEditMode);
    }

    private void setupEventListeners(String uid) {

        DatabaseReference ref = FirebaseDatabase.getInstance().getReference("users").child(uid);

        ref.child("info").addValueEventListener(mInfoListener);
        ref.child("listings").child("active").addValueEventListener(mActiveListingListener);
        ref.child("bookings").child("active").addValueEventListener(mActiveBookingListener);
        ref.child("rentals").child("active").addValueEventListener(mActiveRentalListener);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        String uid = FirebaseAuth.getInstance().getCurrentUser().getUid();
        DatabaseReference ref = FirebaseDatabase.getInstance().getReference("users").child(uid);

        ref.child("info").removeEventListener(mInfoListener);
        ref.child("listings").child("active").removeEventListener(mActiveListingListener);
        ref.child("bookings").child("active").removeEventListener(mActiveBookingListener);
        ref.child("rentals").child("active").removeEventListener(mActiveRentalListener);
    }

    private void setupProfilePic() {
        String uid = FirebaseAuth.getInstance().getCurrentUser().getUid();
        File profileFile = ProfileImageDatabase.getProfileImage(uid);
        if (profileFile != null) {
            Log.d(TAG, "setupProfilePic: got from cache!");
            Glide.with(getContext())
                    .fromFile()
                    .load(profileFile)
                    .centerCrop()
                    .placeholder(R.drawable.temp_profile)
                    .crossFade(350)
                    .into(mBinding.profileImage);
        } else {
            Util.getProfilePic(uid, mProfileSuccessListener, mProfileFailureListener);
        }
    }

    private void displaySignOutWarning() {
        new MaterialDialog.Builder(getContext())
                .title(R.string.sign_out_title)
                .content(R.string.ask_sign_out)
                .positiveText(R.string.sign_out)
                .negativeText(android.R.string.cancel)
                .onPositive(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                        SignInUtils.signOut(getActivity().getSharedPreferences(USER_PREFS, Context.MODE_PRIVATE));
                        ((MainActivity) getActivity()).setSelectedNavItem(R.id.map);
                        dialog.dismiss();
                    }
                })
                .onNegative(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                        dialog.dismiss();
                    }
                })
                .show();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    public void setActiveListingCount(int activeListingCount) {
        this.mActiveListingCount = activeListingCount;
    }

    public void setActiveBookingCount(int activeBookingCount) {
        this.mActiveBookingCount = activeBookingCount;
    }

    public void setActiveRentalCount(int activeRentalCount) {
        this.mActiveRentalCount = activeRentalCount;
    }
}
