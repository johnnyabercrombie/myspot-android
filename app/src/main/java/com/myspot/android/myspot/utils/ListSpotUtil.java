package com.myspot.android.myspot.utils;

import android.net.Uri;
import android.support.annotation.NonNull;
import android.util.Log;

import com.esafirm.imagepicker.model.Image;
import com.firebase.geofire.GeoFire;
import com.firebase.geofire.GeoLocation;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.storage.FirebaseStorage;
import com.myspot.android.myspot.models.Listing;

import java.io.File;
import java.util.List;


public class ListSpotUtil {
    public static final String TAG = ListSpotUtil.class.getSimpleName();

    public static void addListing(Listing listing, String user, List<Image> imageList) {
        String listingKey = FirebaseDatabase.getInstance().getReference("listings").push().getKey();
        FirebaseDatabase.getInstance().getReference("listings").child(listingKey).setValue(listing);
        String userKey = FirebaseDatabase.getInstance().getReference("users").child(user).child("listings").push().getKey();
        FirebaseDatabase.getInstance().getReference("users").child(user).child("listings").child(userKey).setValue(listingKey);

        GeoFire geoFire = new GeoFire(FirebaseDatabase.getInstance().getReference("geofire"));
        geoFire.setLocation(listingKey, new GeoLocation(listing.getLocation().getLatitude(), listing.getLocation().getLongitude()));

        for (int i = 0; i < imageList.size(); i++) {
            Image image = imageList.get(i);
            FirebaseStorage.getInstance().getReference("listings")
                    .child(listingKey)
                    .child("image" + i)
                    .putFile(Uri.fromFile(new File(image.getPath())));
        }
    }
}
