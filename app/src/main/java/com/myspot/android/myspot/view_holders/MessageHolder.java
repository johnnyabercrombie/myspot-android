package com.myspot.android.myspot.view_holders;

import android.app.Activity;
import android.databinding.DataBindingUtil;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Gravity;
import android.view.View;

import com.bumptech.glide.Glide;
import com.google.firebase.storage.FileDownloadTask;
import com.myspot.android.myspot.R;
import com.myspot.android.myspot.databinding.ContentMessageBinding;
import com.myspot.android.myspot.models.Message;
import com.myspot.android.myspot.utils.Util;

import java.io.File;


public class MessageHolder extends RecyclerView.ViewHolder {
    private static final String TAG = MessageHolder.class.getSimpleName();

    private Message mMessage;
    private ContentMessageBinding mBinding;
    private boolean isUserMessage;
    private boolean imageSet;
    private Util.OnProfileSuccessListener mProfileListener = new Util.OnProfileSuccessListener() {
        @Override
        public void onSuccess(FileDownloadTask.TaskSnapshot taskSnapshot) {
            if (mBinding != null) {
                setProfileImage(getLocalFile());
            }
            cleanUp();
        }
    };
    private Activity mActivity;

    public MessageHolder(View itemView) {
        super(itemView);
        mBinding = DataBindingUtil.bind(itemView);
    }

    public void bind(Message message, String uID, @Nullable File image, final Activity activity) {
        imageSet = false;
        mMessage = message;
        mActivity = activity;
        isUserMessage = message.getSenderId().equals(uID);

        if (mBinding != null) {
            if (isUserMessage) {
                Log.d(TAG, "bind: setting up User Message (" + message.getMessage() + ")");
                setupUserMessage();
            } else {
                Log.d(TAG, "bind: setting up NonUser Message (" + message.getMessage() + ")");
                setupNonUserMessage();
                setProfileImage(image);
            }
            mBinding.content.setText(mMessage.getMessage());
        } else {
            Log.w(TAG, "bind: binding was null!" + (mMessage == null ? " mMessage was null!" : " mMessage: " + mMessage.getMessage()));
        }
    }

    private void setProfileImage(File image) {
        if (image == null) {
            mBinding.profileImageLeft
                    .setImageDrawable(Util.getDrawableCompat(mBinding.mainView.getContext(), R.drawable.temp_profile));
            Util.getProfilePic(mMessage.getSenderId(), mProfileListener, null);
        } else {
            Log.d(TAG, "bind: have the image!");
            if (!imageSet && mActivity != null && !mActivity.isDestroyed()) {
                Glide.with(mBinding.mainView.getContext())
                        .load(image)
                        .crossFade(350)
                        .into(mBinding.profileImageLeft);
                imageSet = true;
            } else {
                Log.w(TAG, "setProfileImage: image was already set! listener overflow");
            }
        }
    }

    private void setupNonUserMessage() {
        mBinding.timestampRight.setVisibility(View.GONE);
        mBinding.profileImageLeft.setVisibility(View.VISIBLE);
        mBinding.timestampLeft.setVisibility(View.VISIBLE);
        mBinding.timestampLeft.setText(Util.getMessageDateFormatted(mMessage.getDate()));
        mBinding.content.setGravity(Gravity.START);
    }

    private void setupUserMessage() {
        mBinding.profileImageLeft.setVisibility(View.GONE);
        mBinding.timestampLeft.setVisibility(View.GONE);
        mBinding.timestampRight.setVisibility(View.VISIBLE);
        mBinding.timestampRight.setText(Util.getMessageDateFormatted(mMessage.getDate()));
        mBinding.content.setGravity(Gravity.END);
    }

    public void cleanUp() {
        mMessage = null;
        mActivity = null;
    }

}
