package com.myspot.android.myspot.views;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.location.Address;
import android.location.Geocoder;
import android.os.AsyncTask;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.constraint.ConstraintLayout;
import android.support.v4.view.animation.FastOutSlowInInterpolator;
import android.util.AttributeSet;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.location.places.AutocompleteFilter;
import com.google.android.gms.location.places.AutocompletePrediction;
import com.google.android.gms.location.places.AutocompletePredictionBuffer;
import com.google.android.gms.location.places.GeoDataApi;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.Places;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.mancj.materialsearchbar.MaterialSearchBar;
import com.myspot.android.myspot.R;
import com.myspot.android.myspot.databinding.ViewMsSearchBinding;
import com.transitionseverywhere.Slide;
import com.transitionseverywhere.Transition;
import com.transitionseverywhere.TransitionManager;

import java.util.ArrayList;
import java.util.List;

public class MSSearchBar extends MaterialSearchBar {
    public static final String TAG = MSSearchBar.class.getSimpleName();
    public static final int DEFAULT_MAX_SUGGESTION_COUNT = 8;

    public MSSearchBar(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public MSSearchBar(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    public MSSearchBar(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
    }

    public enum Mode{DISABLED, MAP, MESSAGES}

    /**
     * Used to determine what action to take upon searches, etc.
     */
    private Mode mMode;

    private PendingResult<AutocompletePredictionBuffer> mAutoCompleteResults;


//    /**
//     * Searches for locations in the background.
//     * Requires a {@link Context} to build the {@link Geocoder} and automatically releases it
//     * upon calling {@link LocationSearchTask#onCancelled()}.
//     */
//    private class LocationSearchTask extends AsyncTask<String, Void, List<String>> {
//        private Geocoder geocoder;
//
//        public LocationSearchTask(@NonNull Context context) {
//            super();
//            geocoder = new Geocoder(context);
//        }
//
//        @Override
//        protected List<String> doInBackground(String... params) {
//            List<Address> addressList;
//            List<String> addressNames = new ArrayList<>();
//
//            try {
//                addressList = geocoder.getFromLocationName(params[0], MSSearchBar.DEFAULT_MAX_SUGGESTION_COUNT);
//
//                for (Address a : addressList) {
//                    StringBuilder sb = new StringBuilder();
//                    if (a.getFeatureName() != null) {
//                        Log.d(TAG, "doInBackground: feature name is " + a.getFeatureName());
//                        addressNames.add(a.getFeatureName());
//                    } else {
//                        if (a.getMaxAddressLineIndex() >= 0) {
//                            sb.append(a.getAddressLine(0));
//                            if (a.getMaxAddressLineIndex() >= 1) {
//                                sb.append(" ");
//                                sb.append(a.getAddressLine(1));
//                            }
//                            addressNames.add(sb.toString());
//                        }
//                    }
//                }
//            } catch (Exception e) {
//                Log.e(TAG, "LocationSearchTask: " + Log.getStackTraceString(e));
//            }
//
//            Log.d(TAG, "LocationSearchTask: Address Count: " + addressNames.size());
//
//            return addressNames;
//        }
//
//        @Override
//        protected void onPostExecute(List<String> strings) {
//            if (mMode == Mode.MAP && !isCancelled()) {
//                Log.d(TAG, "onPostExecute: setting suggestions with count " + strings.size());
//                updateLastSuggestions(strings);
//            }
//        }
//
//        @Override
//        protected void onCancelled(List<String> strings) {
//            super.onCancelled(strings);
//            geocoder = null;
//        }
//    }

    public void setMapMode() {
        mMode = Mode.MAP;
        if (getVisibility() != VISIBLE) {
            enter();
        }
    }

    public void setMessagesMode() {
        mMode = Mode.MESSAGES;
        if (getVisibility() != VISIBLE) {
            enter();
        }
    }

    public void disable() {
        mMode = Mode.DISABLED;
        if (getVisibility() == VISIBLE) {
            exit();
        }
    }

    private void enter() {
        Log.d(TAG, "enter: ");
//        Transition slideIn = new Slide(Gravity.TOP);
//        slideIn.setInterpolator(new FastOutSlowInInterpolator());
//        slideIn.setDuration(350);
//        slideIn.setStartDelay(300);
//        TransitionManager.beginDelayedTransition(((ViewGroup) getRootView()), slideIn);
        setVisibility(VISIBLE);
        //setMargins(0, px, 0 ,0);
        //setTranslationY(Util.dpToPx(32, getContext()));
    }

    private void exit() {
        Log.d(TAG, "exit: ");
//        Transition slideOut = new Slide(Gravity.TOP);
//        slideOut.setInterpolator(new FastOutSlowInInterpolator());
//        slideOut.setDuration(350);
//        slideOut.setStartDelay(300);
//        slideOut.addListener(new Transition.TransitionListener() {
//            @Override
//            public void onTransitionStart(Transition transition) {
//
//            }
//
//            @Override
//            public void onTransitionEnd(Transition transition) {
//                setVisibility(GONE);
//            }
//
//            @Override
//            public void onTransitionCancel(Transition transition) {
//
//            }
//
//            @Override
//            public void onTransitionPause(Transition transition) {
//
//            }
//
//            @Override
//            public void onTransitionResume(Transition transition) {
//
//            }
//        });
//        TransitionManager.beginDelayedTransition(((ViewGroup) getRootView()), slideOut);
        setVisibility(GONE);
    }

    public void cleanUp() {
        if (mAutoCompleteResults != null) {
            mAutoCompleteResults.cancel();
            mAutoCompleteResults = null;
        }
    }

    public void getLocationSuggestions(String locString, GoogleApiClient apiClient, LatLng latLng) {
        if (mAutoCompleteResults != null) {
            mAutoCompleteResults.cancel();
        }

        mAutoCompleteResults = Places.GeoDataApi.getAutocompletePredictions(apiClient, locString,
                new LatLngBounds(latLng, new LatLng(latLng.latitude + 2, latLng.longitude + 2)),
                new AutocompleteFilter.Builder().setTypeFilter(AutocompleteFilter.TYPE_FILTER_NONE)
                        .build());
        mAutoCompleteResults.setResultCallback(new ResultCallback<AutocompletePredictionBuffer>() {
            @Override
            public void onResult(@NonNull AutocompletePredictionBuffer autocompletePredictions){
                if (mMode == Mode.MAP) {
                    List newList = new ArrayList();
                    for (AutocompletePrediction o : autocompletePredictions) {
                        newList.add(o.getPrimaryText(null));
                    }
                    Log.d(TAG, "onPostExecute: setting suggestions with count " + autocompletePredictions.getCount());
                    updateLastSuggestions(newList);
                }
            }
        });
        Log.d(TAG, "getLocationSuggestions: getting suggestions for " + locString);
    }
}
