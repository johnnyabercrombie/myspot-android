package com.myspot.android.myspot.databases;


import android.os.Build;
import android.util.ArrayMap;

import java.io.File;
import java.util.HashMap;
import java.util.Map;

public class ProfileImageDatabase {
    private static final int MAX_IMAGE_COUNT = 20;

    private static final Map<String, Integer> mCountMap = Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT
            ? new ArrayMap<String, Integer>()
            : new HashMap<String, Integer>();
    private static final Map<String, File> mImageMap = Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT
            ? new ArrayMap<String, File>()
            : new HashMap<String, File>();

    public static void addProfileImage(String uID, File image) {

        if (MAX_IMAGE_COUNT == mImageMap.size()) {
            removeLeastUsed();
        }
        mImageMap.remove(uID);
        mImageMap.put(uID, image);
    }

    private static void removeLeastUsed() {
        int lowest = 0;
        String lowestStr = "";

        for (String s : mCountMap.keySet()) {
            if (mCountMap.get(s) < lowest) {
                lowest = mCountMap.get(s);
                lowestStr = s;
            }
        }

        mCountMap.remove(lowestStr);
        mImageMap.remove(lowestStr);
    }

    public static File getProfileImage(String uID) {
        return mImageMap.get(uID);
    }


}
